---
title: "Plasma Mobile Gear 21.12 is Out"
subtitle: New apps, more features, improved design, better performance
SPDX-License-Identifier: CC-BY-4.0
date: 2021-12-07
author: Plasma Mobile Team
authors:
  - SPDX-FileCopyrightText: 2021 Bhushan Shah <bshah@kde.org>
  - SPDX-FileCopyrightText: 2021 Aleix Pol <aleixpol@kde.org>
  - SPDX-FileCopyrightText: 2021 Alexey Andreev <aa13q@ya.ru>
  - SPDX-FileCopyrightText: 2021 Bart De Vries <bart@mogwai.be>
  - SPDX-FileCopyrightText: 2021 Carl Schwan <carl@carlschwan.eu>
  - SPDX-FileCopyrightText: 2021 Claudio Cambra <claudio.cambra@gmail.com>
  - SPDX-FileCopyrightText: 2021 Devin Lin <devin@kde.org>
  - SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>
  - SPDX-FileCopyrightText: 2021 Michael Lang <criticaltemp@protonmail.com>
  - SPDX-FileCopyrightText: 2021 Aniqa Khokhar <aniqa.khokhar@kde.org>
  - SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
  - SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
navbar:
  - name: Clock
    url: navbar2.png
  - name: Kasts
    url: navbar1.png
  - name: Dialer
    url: navbar3.png
  - name: Discover
    url: navbar4.png
  - name: Tokodon
    url: navbar5.png
kweather:
  - name: Flat View
    url: kweather1.png
  - name: Dynamic View
    url: kweather2.png
  - name: Dynamic View on Desktop
    url: kweather3.png
qmlkonsole:
  - name: Main View
    url: qmlkonsole1.png
  - name: Tabs List
    url: qmlkonsole2.png
koko:
  - name: Pictures Page
    url: koko1.png
  - name: Places Page
    url: koko2.png
  - name: Share dialog on mobile
    url: koko-share.png
tokodon:
  - name: Tokodon login page
    url: tokodon-login.png
  - name: Tokodon home
    url: tokodon-home.png
  - name: Tokodon desktop
    url: tokodon-desktop.png
  - name: Tokodon settings
    url: tokodon-settings.png
kasts:
  - name: Bottom toolbar in the mobile version
    url: kasts-bottom-toolbar.png
  - name: Chapters in the mobile player
    url: kasts-chapter-marks-player.png
  - name: Chapters in the episode details view
    url: kasts-chapter-marks-details.png
  - name: New filter capabilities on episode list view
    url: kasts-filter-episodes.png
  - name: Selection and contextual actions on mobile version
    url: kasts-selection-actions-desktop.png
  - name: Selection and context menu on desktop version
    url: kasts-selection-actions-mobile.png
  - name: Categorized settings on mobile
    url: kasts-settings-mobile.png
  - name: gpodder.net syncing
    url: kasts-gpodder-mobile.png
spacebar:
  - name: MMS messages
    url: spacebar1.png
  - name: Group message
    url: spacebar2.png
  - name: Colors and font sizes
    url: spacebar3.png
dialer:
  - name: New notification format
    url: dialer-notification-1.png
  - name: New notification format
    url: dialer-notification-2.png
kalendar:
  - name: Kalendar Month View
    url: kalendar-month.png
  - name: Kalendar Week View
    url: kalendar-week.png
  - name: Kalendar Sidebar
    url: kalendar-sidebar.png
---

The Plasma Mobile team is happy to introduce the Plasma Mobile updates for September to December 2021.

## ModemManager

Since its inception, Plasma Mobile has used the oFono stack for telephony functions (mobile data, calling, SMS), but with this Plasma and Gear release, we are transitioning our telephony stack to ModemManager.

### Context

**oFono** is a Nokia/Intel project started in 2009 with the Nokia N900. It integrates with the higher-level [ConnMan](https://git.kernel.org/pub/scm/network/connman/connman.git/about/) connection manager, and is currently used by projects like Ubuntu Touch and Sailfish, which maintain their own series of patches on top of the stack in order for it to work for their use cases.

**ModemManager** is a FreeDesktop project started in 2008 with the goal of providing USB dongle support for desktops. It integrates with the higher-level NetworkManager network management daemon. It is currently used on Plasma Desktop and the GNOME desktop to provide support for USB modems, as well as on Phosh for telephony functions.

### Why we are switching

oFono and ModemManager share similar goals in providing a unified API for accessing modem devices via D-Bus. However, the pace of development has been slow upstream for oFono, with our usage depending on a series of patches in order to make it work.

ModemManager has generally been better in that regard, with more active development and new devices being upstreamed.

The main drawback of switching off of oFono is that it is the only option for Halium devices. However, due to our recent decision to drop Halium support, this factor is no longer a constraint.

On mainlined devices, such as the PinePhone and the OnePlus 6, ModemManager has also proved to be much more reliable with handling the modem, due to a more active development than oFono on that front.

Our goal is to provide the best possible experience on mainline Linux devices, so using the ModemManager stack will allow us to deliver that for telephony functions.

This also allows us to use the same stack as on Plasma Desktop, allowing for better integration with Plasma Desktop and for applications like Dialer and Spacebar to be used on it.

### Status

At this moment, we have completed our initial switch to ModemManager. However, the code has not been extensively tested, which means features and fixes will still be rolling out on each monthly Plasma Mobile Gear release.

---

## Shell

The shell has seen major improvements in the 5.23 development cycle (June to September), especially with regard to KWin.

### Keyboard

Rodney worked on the keyboard (maliit). He fixed keyboard hints, so that apps can now open certain types of keyboards (for example, the numerical pad keyboard).

Aleix fixed a lot of behavior issues regarding when the keyboard should and shouldn't be opened, while also correcting keyboard disappearing issues.

Besides working as expected on the phone, now Plasma will be considering whether to expose the virtual keyboard depending on whether we just used touch to open, or, alternatively, a pointer device like a mouse.

### External Displays

Aleix fixed connecting a Plasma Mobile powered phone to an external display. There was a problem in KWin where we would be requesting more video memory than necessary. This would lead to a failure on the PinePhone. As this has now been fixed, Plasma can deal with many more displays across the board. Connecting to an external screen shouldn't be a problem anymore as long as its resolution is supported by the phone itself. If you notice any problems, try lowering the resolution or the refresh rate in the _Displays_ configuration module.

Aleix also added a new button to the previews of the apps displayed when you hit the _Plasma_ control. On the previews of open apps, set next to the close icon, this new button lets send an app to an external screen when pressed.

And, starting with Plasma 5.24, we will also be able to use the _Primary Output_ concept that will help us ensure the main components stay in the output we want. Thanks to these changes, we should be able to use our apps on external screens along with the comfort of external keyboards, mouse and the like. It is also now possible to switch to the classic Plasma look and feel we are used to on desktop systems, but note that we do not have automatic mechanisms to switch back to "phone look" just yet.

### Top Panel

Aleix reworked the internals of the top panel quick settings, making them extensible. It is now possible to create custom quick settings. He also added the ability to open the clock app when tapping on the top panel clock label, and fixed freezing issues in the quick settings panel which happened with certain scaling. He also adjusted the buttons' appearance.

Tobias ported the mobile data indicator to ModemManager, and Bhushan added the airplane mode quick setting. Meanwhile, Devin reworked the layout of the top panel elements to better support notches/punchhole cameras in the future (Plasma 5.24 only).

![New Top Panel Layout (for Plasma 5.24)](toppanel.png)

### Task Panel

Aleix worked on having the bottom task panel move to the side on landscape, saving vertical space.

![Task Panel in Landscape](landscapetaskpanel.png)

### Other

Aleix integrated the XDG Activation Wayland protocol, so that there are better window animations for application startup, and added smooth fading for turning on and off the screen. He also added screen rotation animations.

## Kirigami

Devin introduced the `NavigationTabBar` component, which allows for applications to have navigation from a bottom toolbar, as opposed to a sidebar. This was upstreamed from the navbar used in the clock and dialer apps. Elisa, Discover, Tokodon and Kasts are now all using it.

{{< screenshots name="navbar" >}}

Devin also set sidebar handles to be in the header by default, so that navbars do not cover them.

## KWeather

Devin rewrote the dynamic view and location switching behavior. As a result, the dynamic view is vastly more performant, and can now run at >30fps on the PinePhone, compared to the <5fps from before. Devin also reworked user navigation throughout the app, removing the sidebar completely from mobile.

{{< screenshots name="kweather" >}}

## Image Viewer (Koko)

The biggest visual change in Koko is the introduction on mobile of a bottom navigation bar. This makes it sensibly faster to navigate Koko on your phone. The navbar comes with a new overview page that contains all the previously displayed images inside the sidebar sections, allowing you to filter by location, date, network folders, etc. This feature was implemented by Devin
and Mikel Johnson.

Carl worked on a new share dialog. On mobile, it will appear as a drawer, and on the desktop, it will use the classical drop down menu.

{{< screenshots name="koko" >}}

Noah worked on the integrated image editor and introduced a new functionality: image resizing. He also improved the existing cropping feature. You can read more about it on [KQuickImageEditor 0.2 release announcement](https://carlschwan.eu/2021/10/02/kquickimageeditor-0.2-released/).

Noah also worked on a slideshow sidebar available on the desktop when hovering over the window.

{{< video src="koko-hoveringtoolbar-2021-09-11_09.42.36.webm" >}}

Behind the scenes, there were also many improvements. Mikel worked on [improving the rendering of SVG files](https://invent.kde.org/graphics/koko/-/merge_requests/82), and
Arjen added [color correction on X11](https://invent.kde.org/graphics/koko/-/merge_requests/89).

Finally, on the desktop, Koko will now remember its position and size.

## Web Browser (Angelfish)

Swapnil Tripathi added a button to clear the browser history. Rodney Dawes improved the virtual keyboard integration, so that the keyboard only shows URL characters when entering text into the URL bar. Valerii Vtorygin added a popup that allows you to ignore SSL errors, and fixed loading `chrome://` URLs.

In addition to the network based blocking, Jonah Brüchert improved the ad blocker to support cosmetic filters. These hide elements of the page.

Additionally, some code modernization work was done.

## Terminal Emulator (QMLKonsole)

QMLKonsole's interface got a massive overhaul, as Devin added tabs (as well as a swipe gesture to switch between them), and a keyboard toggle button, which now controls keyboard visibility in the app.

{{< screenshots name="qmlkonsole" >}}

## KClock

Devin moved the settings button from the navbar to the header actions to increase touch targets for the other pages. He also switched the navbar from a custom component to use the new `NavigationTabBar` component.

Han changed the behavior of the system panel notify item, and now the notify item only shows itself when alarms are pending. Also, KClockd will exit after 30s when no active alarm and timer is present. But, if KClock is running, KClockd won't exit. If you find that KClock can no longer wake the phone from sleep, please open an issue at https://invent.kde.org/plasma-mobile/kclock.

## Kasts

A lot has happened since the previous release. To begin with, Swapnil and Tobias have added support for chapters. This includes both chapters provided in the RSS feed, as well as chapters encoded into the mp3 tags. These will show up on the episode's "details" page and in the header, player controls on desktop. On mobile, these will also appear on the player controls through an additional swipe-able page. Tobias then reworked the settings, splitting them up into separate categories. Settings will now open in a separate window on the desktop.

On mobile, Devin replaced the global menu with a bottom toolbar, and moved the position of the contextual action handle and actions to the top toolbar.

The subscription page is now sorted based on the number of unplayed episodes, with equally-ranked podcasts being sorted alphabetically.

Bart put in a lot of work, starting with his tweaks to the episode page: instead of having tabbed pages, it now consists of a single page to which filters can be applied (e.g. all episodes, played episodes, new episodes, etc.). He also added multi-selection and contextual action support to the subscription page and all episode list views. Items can now be selected using the keyboard, mouse or touch (with a long press). Actions can be applied to the selected items through the context menu on mobile or through right-clicking on the desktop. All lists are now also keyboard-navigable.

Then he worked on optimizing and improving the app's backend. One of the immediate results is that adding a new subscription or refreshing existing ones is now markedly faster (up to ~10x). And, last but not least, Bart added support for syncing subscriptions and episode play positions using the [gpodder.net](https://gpodder.net) service or the [nextcloud-gpodder](https://github.com/thrillfall/nextcloud-gpodder) app on a (self-hosted) Nextcloud server! 

{{< screenshots name="kasts" >}}

## Tokodon

Carl worked on improving the desktop experience a bit by polishing the sidebar that is now displayed automatically when there is enough space and shows the accounts avatars. He also enabled spellchecking and started adding some setting pages for the accounts management and the newly introduced spellchecking feature.

Meanwhile, Nicolas enabled the new GitLab CI, and Devin ported Tokodon to the new NavigationTabBar component.

Finally, a touch of style: Bugsbane made a logo for Tokodon!

![Tokodon logo](tokodon-logo.png)

{{< screenshots name="tokodon" >}}

## Kalendar

Claudio Cambra and other contributors continued to work on Kalendar, adding new features, cleaning up papercuts and stomping on bugs. You can find an overview of their progress at [claudiocambra.com](https://claudiocambra.com).

{{< screenshots name="kalendar" >}}

## SMS/MMS (Spacebar)

Spacebar now has full MMS support! Michael Lang integrated MMS handling directly within Spacebar, and it is now able to send/receive group chats, images and other attachments. You can also send several attachments within a single message. Images are automatically resized if they exceed the maximum message size, and the various MMS settings can be configured directly from within Spacebar's settings. Among other things, you can configure Spacebar to either auto download all MMS messages, or instead, receive a download indicator message allowing you to choose when/if to download each MMS message.

Nicolas Fella switched the backend from oFono to ModemManager. Switching to ModemManager fixed an issue where emojis were not being sent or received correctly. The switch also made the addition of MMS messaging much easier.

And Jonah Brüchert introduced a new PhoneNumber class. This streamlines the handling of phone numbers and prevents various formatting inconsistencies/mismatches.

More new features in Spacebar include:

- Options to customize the appearance of the chat bubble colors and message font size.
- The ability to delete individual messages.
- The ability to extract links and 2FA codes from within a message's text and copy them to the clipboard.
- The ability to resend a failed message.
- Disabled chat notifications for chats the user is viewing.
- Chat lists that show the date/time of the most recent message for each chat.
- A message text input that allows multiple lines.
- Fixed premium number detection.

{{< screenshots name="spacebar" >}}

## Dialer

Alexey Andreev improved the Plasma dialer by splitting it between background services and a GUI client. The background service was then ported from oFono to ModemManager.

Besides that, Alexey worked on the [developer documentation](https://develop.kde.org/docs/plasma-mobile/telephony/) for the Plasma Mobile telephony stack. He also added haptic feedback (vibrations) to the incoming call notification and improved the notification format to show not only the recognized contact name, but also the actual phone number.

{{< screenshots name="dialer" >}}

## NeoChat

Starting with this release, NeoChat is part of the monthly Plasma Mobile releases. NeoChat's prior release was roughly six months ago, so this one contains tons of new features and fixes. For example, we have introduced spellchecking for the input box, and custom emojis and fixes to the timeline layout, as well as to various problems that occurred during startup, shutdown and account switching.

## Contributing

Want to help with the development of Plasma Mobile?

If you would like to take Plasma Mobile for a spin, read this page to see device support for each distribution: https://www.plasma-mobile.org/get/

We now have a general issue tracker, where you can report any issues you encounter here: https://invent.kde.org/teams/plasma-mobile/issues/-/issues

Also, consider joining our [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org), and let us know what you would like to work on!
