---
title: "Plasma Mobile: More Applications and an Improved Homescreen"
subtitle: Plasma Mobie in March and April 2021
SPDX-License-Identifier: CC-BY-4.0
date: 2021-06-10
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
- SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>
- SPDX-FileCopyrightText: 2021 Bhushan Shah <bshah@kde.org>
- SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
- SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
tokodon:
  - name: Tokodon Home timeline
    url: tokodon.png
  - name: Tokodon Send a post
    url: tokodon-send.png
  - name: Tokodon create a poll
    url: todon-choice.png
  - name: Tokodon profile info
    url: tokodon-profile.png
dialer:
  - name: Dialer lightweight fonts
    url: dialer-lightfont.png
  - name: Dialer long numbers
    url: dialer-longnum.png
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
---

The Plasma Mobile team is happy to present the Plasma Mobile updates for May 2021.

## Applications

From now on, KDE applications for Plasma Mobile will be released monthly, a.k.a the Plasma Mobile Gear. Please see the announcement for the first release, Plasma Mobile Gear v21.05 [here](https://www.plasma-mobile.org/2021/05/10/plasma-mobile-gear-may-2021/).

Having a regular release cycle lets us bring more applications to [Flathub](https://flathub.org). Expect to see them being added over the next couple of weeks!

## Shell

At long last, Marco's work on multiple homescreen pages has been merged! :tada: Many homescreen components have been isolated, but work still needs to be done on allowing users to switch homescreens.

Devin worked on adding a more powerful audio overlay to the shell, allowing for quickly controlling application volumes, switching inputs/outputs, and controlling their volumes.

![Audio Overlay](audio-osd.png)

The look of the search applet was also improved.

![Search applet](search-applet.png)

Work is also underway on a list version of the homescreen drawer.

![List drawer](list-drawer.png)

In other news, the lockscreen now features a keyboard toggle, allowing you to use passwords containing text instead of numbers only, the notifications panel had a bug fixed where "Do not disturb" and "Clear all" buttons were not showing, and the SIM pin dialog now follows the system's color scheme and looks at home with a dark theme.

## KClock

Swapnil added a feature that lets you create timer presets. This allows you to quickly set timers for recurring things, such as making tea. He also added a +1 minute button to the timer page that let's you quickly increase the duration of the running timer.

![KClock timer](kclock-timer.png)

Devin worked on improving the KClock Plasmoid, reworking the appearance and adding a settings page to customize it.

![KClock plasmoid on the homescreen](kclock-plasmoid.png)

He also added error messages in the application that warns you if the timer/alarm daemon is not running in the background.

## Kalk

Quite a few improvements have landed in the Plasma Mobiles calculator app, among them, Kalk now recognizes the system locale's decimal separator, pressing **=** to add the result to the input field won't cause parse error on locales (like German) anymore, and fractions are now output as float types.

Also, thanks to Devin Lin, Kalk is now on [Flathub](https://flathub.org/apps/details/org.kde.kalk) and Manuel redesigned the application's icon.

![Kalk icon](kalk.png)

## Phonebook

The phonebook application now uses a toolbar to display actions such as calling or texting a contact. This frees up space that can now be used to display more information about a contact, such as a contact's birthday, which is now shown. More information will be added in the future, so, if you are looking for an easy way to contribute to Plasma Mobile, this is it!

Furthermore, scrolling the contact list is much smoother now, and the wording throughout the app is more consistent.

![Phonebook application](phonebook-toolbar.png)

## Dialer

The dialer application gained plenty of UI improvements from Smitty van Bodegom, Nicolas Fella, and Bhushan Shah. For example, to avoid accidental calls tapping an entry in the contact list, it now fills the dialpad with the number instead of directly calling the contact right away. You can now query your phone's IMEI number by calling the special `*#06#` number, and long-pressing the 1 key now takes you to your voicemail. The incoming call notification and the in-call view now show your contact's name and photo instead of the phone number. It is now possible to mute yourself and switch audio to the speakers.

{{< screenshots name="dialer" >}}

## Tokodon

Carl worked on a new Mastodon client called Tokodon. For the moment Tokodon allows you to browse the home, local and global timeline; show account information; post toots and replies (with attachments) and lets you use it with multiple accounts at the same time.

{{< screenshots name="tokodon" >}}

## Spacebar

Smitty van Bodegom implemented opening SMS links in spacebar. These can be used by other apps, for example, the phonebook, to open Spacebar in a specific chat. He also added a warning that informs the user if they are trying to text a premium SMS number.
Spacebar now sorts the chats by the last contacted time, thanks to a merge request by Caleb McCarthy, and you can now click on the links in an SMS.

## Alligator

Tobias fixed a small but prominent bug where the size of images was distorted in some cases.

## Kasts

Another new kid on the block is Kasts, a podcast player app. Kasts started off as a fork of Alligator, but it currently supports a play queue for episodes, has lockscreen and notification panel media controls on Linux, and will prevent the system/phone from suspending while listening to podcasts.

This is what it looks like on a phone.

![Kasts podcast player on mobile](kasts-mobile.png)

And this is the optimized UI on the desktop.

![Kasts podcast player on desktop](kasts-desktop.png)

## Calindori

In our convergent calendar app, when you trigger the deletion of an incidence, a confirmation message is displayed on top of the content and it also disables the rest of the actions available. Moreover, on the desktop, no more than two page columns are visible at the same time.

## KWeather

KWeather now works properly when running as a Flatpak. Also, the locations page was improved to properly handle long location names so they do not overflow out of the screen, and the dynamic weather forecast page was improved to have a maximum width set with widescreen layouts.

After some changes in KWeather and KWeatherCore, the config and cache format has been changed. If you update from the old version, please remove the following files/directories (if they are present):

* `~/.config/kweatherrc`
* `~/.config/kweather.conf`
* `~/.config/kweather/kweatherrc`
* `~/.cache/kweather`

As a result, you may need to re-add locations and re-config KWeather.

## Settings

The settings application now includes more of the modules that are already in the Plasma Desktop version. These include *Autostart*, *Night Color*, *Displays*, *Software Update*, *Notifications*, and *Lockscreen*.

## Angelfish

Angelfish has gained a web app manager as a central place to remove installed web apps. Previously it was only possible to remove them by going to the site from which they were added.

## Kongress

The schedule of Akademy 2021, KDE's annual world summit, is now available in Kongress.

## Contributing

Want to help with the development of Plasma Mobile? 

If you would like to take Plasma Mobile for a spin, read this page to see device support for each distribution: https://www.plasma-mobile.org/get/

We now have a general issue tracker, where you can report any issues you encounter here: https://invent.kde.org/teams/plasma-mobile/issues/-/issues

Also, consider joining our [matrix channel](https://matrix.to/#/#plasmamobile:matrix.org), and let us know what you would like to work on!
