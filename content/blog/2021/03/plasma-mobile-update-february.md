---
SPDX-License-Identifier: CC-BY-4.0
author: Plasma Mobile team
authors:
- SPDX-FileCopyrightText: 2021 Alexey Andreyev <aa13q@ya.ru>
- SPDX-FileCopyrightText: 2021 Bhushan Shah <bshah@kde.org>
- SPDX-FileCopyrightText: 2021 Carl Schwan <carl@carlschwan.eu>
- SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>
- SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
- SPDX-FileCopyrightText: 2021 Han Young <hanyoung@protonmail.com>
- SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb.prv@gmx.de>
- SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@gmx.de>
- SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
created_at: 2021-03-01 14:00:00 UTC
date: "2021-03-01T00:00:00Z"
elisa:
- name: Tracks
  url: /img/post-2021-02/elisa-tracks.png
- name: Player
  url: /img/post-2021-02/elisa-player.png
- name: Albums
  url: /img/post-2021-02/elisa-albums.png
- name: Menu
  url: /img/post-2021-02/elisa-menu.png
- name: Metadata Editor
  url: /img/post-2021-02/elisa-metadata.png
- name: Now Playing
  url: /img/post-2021-02/elisa-now-playing.png
- name: Playlist
  url: /img/post-2021-02/elisa-playlist.png
- name: Sidebar
  url: /img/post-2021-02/elisa-sidebar.png
- name: Running on the Pinephone
  url: /img/post-2021-02/elisa-pinephone.png
- name: Widescreen
  url: /img/post-2021-02/elisa-wide.png
- name: Widescreen Player
  url: /img/post-2021-02/elisa-wide-player.png
keysmith:
- name: Create new account
  url: /img/post-2021-02/keysmith.png
- name: Unlock account
  url: /img/post-2021-02/keysmith-unlock.png
- name: No Account Page
  url: /img/post-2021-02/keysmith-noaccount.png
title: Plasma Mobile February Update
ussd:
- name: USSD message
  url: /img/post-2021-02/ussd-normal.png
- name: USSD response
  url: /img/post-2021-02/ussd-response.png
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
---

The Plasma Mobile team is happy to present the Plasma Mobile updates from January and February 2021.

## Homescreen

Marco has reworked the homescreen so that the drawer behaves solely as an applications list. (Marco Martin: [plasma/plasma-phone-components!126](https://invent.kde.org/plasma/plasma-phone-components/-/merge_requests/126))

He has also laid the groundwork for horizontal pages for widgets and applications, as well as the possibility of developing new custom launchers in the future.

<div class="embed-responsive embed-responsive-16by9">
  <video src="/img/post-2021-02/homescreen.mp4" controls autoplay=true muted></video>
</div>

## Dialer

The highlights for Plasma Dialer include one bug fix and two new features. First off, Powerdevil no longer suspends calls. (Bhushan Shah: [plasma-mobile/plasma-dialer!31](https://invent.kde.org/plasma-mobile/plasma-dialer/-/merge_requests/31)) The two new features are that it is now possible to send [DTMF tones](https://en.wikipedia.org/wiki/Dual-tone_multi-frequency_signaling) and send [USSD requests](https://en.wikipedia.org/wiki/Unstructured_Supplementary_Service_Data). DTMF tones can be sent during a call and can be used to navigate the menus of certain automated calling systems. USSD requests, also called "quick codes", can be used to request the current pre-paid balance from the mobile phone operator, as well as other data. Both features were tested to work on the Pinephone. (Alexey Andreyev: [plasma-mobile/plasma-dialer!32](https://invent.kde.org/plasma-mobile/plasma-dialer/-/merge_requests/32) and [plasma-mobile/plasma-dialer!33](https://invent.kde.org/plasma-mobile/plasma-dialer/-/merge_requests/33))

The new features required not only work on the Plasma Dialer side, but also work on the underlying software stack. Several patches have been produced that have been shared among several mobile Linux distributions or if possible directly in the upstream software. For more details please see these commits and merge requests for DTMF: [Manjaro-ARM](https://gitlab.manjaro.org/manjaro-arm/packages/community/plasma-mobile/ofono/-/commit/d5dc98f6492ad666498cea21f717723e52d68db2), [postmarketOS](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/1922), [ubports](https://github.com/ubports/ofono/pull/17) and these for USSD: [telepathy-ofono](https://github.com/TelepathyIM/telepathy-ofono/pull/13), [Manjaro-ARM](https://gitlab.manjaro.org/manjaro-arm/packages/community/plasma-mobile/ofono/-/commit/913fb10663f6651980f2b881e3889fb3a4bb192f), [ubports](https://github.com/ubports/ofono/pull/20), [postmarketOS](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/1976), [Linux kernel](https://git.kernel.org/pub/scm/network/ofono/ofono.git/commit/?id=ce86a6c207222fd5c42b53bc6c4a39c545f543de).

Future work on Plasma Dialer includes supporting [USSD direct responses](https://invent.kde.org/plasma-mobile/plasma-dialer/-/issues/11) and [device special codes](https://invent.kde.org/plasma-mobile/plasma-dialer/-/issues/12).

{{< screenshots name="ussd" >}}

## Spacebar

Spacebar now shows messages with an alphabetical sender address correctly. (Jonah Brüchert: [plasma-mobile/spacebar!12](https://invent.kde.org/plasma-mobile/spacebar/-/merge_requests/12))

## Settings

The appearance KCM has been dropped in favor of using the desktop KCMs for changing the appearance.
(Marco Martin: [plasma/plasma-workspace!631](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/631)),
(Bhushan Shah: [plasma-mobile/plasma-settings!81](https://invent.kde.org/plasma-mobile/plasma-settings/-/merge_requests/81))

A theme selector has been added to the virtual keyboard settings. (Devin Lin:
[plasma-mobile/plasma-settings!82](https://invent.kde.org/plasma-mobile/plasma-settings/-/merge_requests/82))

## KWeather

KWeather has been switched from QtCharts to KQuickCharts. However, due to a bug with the PinePhone GPU driver, on some devices the line can be too thick. We are currently looking for a solution to this problem.

We also extracted our data fetching code to a new library, KWeatherCore. This allows sharing it with other consumers of the same data, allowing KWeather to focus on the user experience. Support for OpenWeatherMap was removed.

Local time is displayed on the forecast page and the location drag animation has been made smoother.

## Kalk

Kalk's unit converter has been redesigned. Some syntax rules for math expressions have been corrected.

## KClock

KClock has received some visual improvements and bug fixes. The time page now uses Plasma's analog clock and does not always highlight a random list entry.

The stopwatch page has received improvements to allow users to see the progress of the current lap.

Bugs have been fixed where new alarms would not persist between reboots unless they were edited.

![KClock time page](/img/post-2021-02/kclock-time-page.png)

## Elisa

Devin Lin has been doing ongoing work on porting Elisa to a mobile format for phones and tablets.
([multimedia/elisa!205](https://invent.kde.org/multimedia/elisa/-/merge_requests/205))

{{< screenshots name="elisa" >}}

## Angelfish

Angelfish has seen a major code clean up lately, partly because it's going through the KDE Review right now.

A few features and bug fixes have also been made:

- Downloaded files can now be opened in external apps.
- Angelfish now opens for all https and http urls, not only for html pages.

## Calindori

Users can now directly open calendar files and links, and they can import data -tasks and events- from files to any existing calendar as well as export calendar data.

Attendees management features have also been implemented. For attendees to work, the calendar must be an external ical file and the calendar owner information should be populated. Email notifications to attendees are not natively supported by the application, but if the external calendar file is synchronized with an external provider (e.g. Nextcloud), the attendees will be informed by email whenever needed. Moreover, the contacts found on the running system can be added as event attendees.

The user interface of the event and task editor pages has also been revamped, and now presents a tab-based design.

![Event editor](/img/post-2021-02/calindori-event-editor.png)

Finally, on the week and day views, the actions responsible for creating new events and tasks have been added into each day or hour row and the display of the event and task items is now more compact.

![Day view](/img/post-2021-02/calindori-day-view.png)

## Alligator

When reading a long article it is common to need to adjust the font size. To solve this issue, a set of configuration options have been added to offer this functionality. Moreover, the Alligator settings are immediately applied, without the user having to manually apply them.

![Alligator settings](/img/post-2021-02/alligator-settings.png)


## Koko

Carl made a bunch of improvements in Koko in the past few days.
The sidebar is now much nicer and looks similar to that of Dolphin.

![Koko on the desktop](/img/post-2021-02/koko-sidebar.png)

A built in image editor was also added using the KQuickImageEditor
library (also used in NeoChat).

<div class="embed-responsive embed-responsive-16by9">
  <video src="/img/post-2021-02/koko-image-editor.mp4" controls autoplay=true muted></video>
</div>

## NeoChat

Tobias and Carl released NeoChat 1.1. You can read all about this release on
[Carl's blog](https://carlschwan.eu/2021/02/23/neochat-1.1/).

Another big piece of news is that Alexey Andreyev added supports for fancy animations when receiving
special emojis. This is similar to Element and can be disabled. Videos of the
effects can be viewed [here](https://i.imgur.com/Z9vF9px.mp4) and [here](https://i.imgur.com/3Ab79yV.mp4).

## Kirigami Addons

The DatePicker component has been visually updated.
(Carl Schwan: [libraries/kirigami-addons!9](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/9))

![DatePicker component](/img/post-2021-02/calendar-big.png)

## Keysmith

The visual interface for the login and account creation has been improved (Devin Lin: [utilities/keysmith!79](https://invent.kde.org/utilities/keysmith/-/merge_requests/79)), and now there is a placeholder message for when no accounts are added.
(Devin Lin: [utilities/keysmith!80](https://invent.kde.org/utilities/keysmith/-/merge_requests/80))

{{< screenshots name="keysmith" >}}

## Discover

Aleix worked on greatly improving the performance of Discover. It has been ported
away from QAction (Aleix Pol: [plasma/discover!66](https://invent.kde.org/plasma/discover/-/merge_requests/66/))
and some string copies are now prevented (Aleix Pol: [plasma/discover!71](https://invent.kde.org/plasma/discover/-/merge_requests/71))

Additionally an Alpine/pmOS backend has been added, making it more
useful to use Discover on these distros.

## Kirigami Documentation

A brand new tutorial was created to help newcomers create convergent applications.
It can be found on [develop.kde.org](https://develop.kde.org/docs/kirigami/).
(Clau Cambra: [documentation/develop-kde-org!67](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/67))

## Want to help?

This post shows what a difference new contributors can make. Do you want to be part of our team as well? We prepared [a few ideas for tasks new contributors](https://invent.kde.org/dashboard/issues/?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Mobile&label_name[]=Junior%20Job) can work on. Most coding tasks require some QML knowledge and [KDAB's](https://www.kdab.com/) [QML introduction video series](https://youtu.be/JxyTkXLbcV4) is a great resource for that!

You can join us on various communication channels using the links at [https://www.plasma-mobile.org/join/](https://www.plasma-mobile.org/join/).
