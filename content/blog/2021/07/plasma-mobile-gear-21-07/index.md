---
title: "Plasma Mobile 21.07 is Out"
subtitle: "Performance improvements, new features and re-designed apps"
SPDX-License-Identifier: CC-BY-4.0
date: 2021-07-20
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2021 Bhushan Shah <bshah@kde.org>
- SPDX-FileCopyrightText: 2021 Bart De Vries <bart@mogwai.be>
- SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@gmx.de>
- SPDX-FileCopyrightText: 2021 Aniqa Khokhar <aniqa.khokhar@kde.org>
- SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
- SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>
- SPDX-FileCopyrightText: 2021 Carl Schwan <carl@carlschwan.eu>
kasts:
  - name: Kasts discover page
    url: kasts-discover.png
  - name: Kasts main menu highlighting
    url: kasts-menu-highlighting.png
  - name: Kasts playback speed setting
    url: kasts-playbackrate.png
  - name: Kasts error message
    url: kasts-error-message.png
  - name: Kasts error list overlay
    url: kasts-error-list.png
  - name: Kasts new settings
    url: kasts-settings.png
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
---

The Plasma Mobile team is happy to present the Plasma Mobile updates for July 2021. Read on to found out about all the new stuff included in this release:

## Shell

First and foremost: Marco Martin made the shell more responsive by improving the performance of the top panel.

## Dialer

We have solved some issues regarding international numbers: When dealing with numbers saved without a country code, we used to need to guess what country the number was from. Previously, we had to do this based on the locale settings, but now we also take into account which country the phone is based in and take information from the cell towers.

We also fixed an issue that could result in the dialer showing the wrong contact name when receiving a call, and now you can also use the dialer correctly with a hardware keyboard.

## Spacebar

Thanks to Smitty van Bodegom and Jonah Brüchert, Plasma Mobile's SMS app gained several UI improvements. For one, errors while sending messages are properly reported, and it also shows you which number you are sending SMS from. Another improvement is that chats are now ordered properly.

## KWeather

Han Young overhauled the KWeather plasmoid. Not only did he fix some annoying issues, but the corrections allow you to select separate locations. The corrections in the plasmoid have also removed the same issues in the app.

As for KWeather's look, Devin worked on giving the dialogs in the app a consistent style, as well as a new inline page indicator for flat mode.

## KClock

Han Young's worked on ensuring that receiving alarms when the phone is suspended works in all cases; and Devin made sure that a proper theme is applied at all times, especially when using KClock on non-Plasma systems. He also changed the dialog style to look more consistent with other native dialogs.

## KRecorder

Talking of which, KRecorder received the same theme fixes applied to KClock, so it also now looks consistent across platforms.

## Qrca

Qrca, Plasma Mobile's QR reader, now allows you to choose between multiple available cameras for scanning barcodes. It also offers to import barcodes for transport tickets into KDE Itinerary, and provides helpful links for barcodes containing International Article Numbers or ISBNs.

We also improved the user interface. The **Share** dialog now handles errors, sends a notification with the shared destination URL for services like Imgur, has a loading indicator and a title. As for the camera selection dialog on the desktop, it won't fill the entire width anymore.

## Calindori

Dimitris Kardarakos made sure that the phone no longer pointlessly wakes up at midnight and added some UI improvements.

## Kasts

It's been quite a busy month for Kasts development. The main features implemented into Plasma Mobile's podcasting app this month include:

- Swapnil implemented a **Discover** page which allows you to search https://podcastindex.org for podcasts.
- Bart added a feature to resume podcast episode downloads. The **Download** page has been adapted to show *downloading*, *partially downloaded* and *completed* download categories.
- Swapnil reworked the playback speed settings. Clicking the speed button will now open an overlay list. This list now includes slower playback speeds.
- Bart added a button to the error notification. This allows you to directly open the list of errors. This error list can also be opened from the **Settings** page (previously this was a tab on the **Downloads** page).
- On systems running NetworkManager, Kasts can now check whether a metered connection is in use (e.g. an LTE connection on the PinePhone). Bart has introduced new settings to allow/disallow checking for podcast updates, downloading episodes or downloading images on such connections.
- Swapnil added highlighting to the currently selected page in the main menu. This should make it easier to navigate.
- Bart added new settings to determine what happens when episodes are marked as played: episodes can be deleted immediately or next time Kasts starts.
- Swapnil added several tooltips. He also added hotkeys: `space` will play/pause playback and `n` will skip to the next track.

Additionally, Bart fixed several bugs. Most notably:

- Inhibit suspend on GNOME and Phosh has been fixed.
- The implementation to restore the playback position of episodes has been improved. There should be no more audible glitches when starting up the app.

{{< screenshots name="kasts" >}}

## In Other News...

The Plasma Mobile sound contest has now concluded. You can find out more [here](/2021/07/08/winners-of-kde-lmms-sound-contest/). A big thank you to all participants! We will be including the sounds in future releases of Plasma Mobile.

## Contributing

Want to help with the development of Plasma Mobile?

If you would like to take Plasma Mobile for a spin, read this page to see device support for each distribution: https://www.plasma-mobile.org/get/

We now have a general issue tracker, where you can report any issues you encounter here: https://invent.kde.org/teams/plasma-mobile/issues/-/issues

Also, consider joining our [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org), and let us know what you would like to work on!
