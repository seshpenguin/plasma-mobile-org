---
SPDX-License-Identifier: CC-BY-4.0
author: Plasma Mobile team
authors:
- SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
- SPDX-FileCopyrightText: 2020 Devin Lin <espidev@gmail.com>
- SPDX-FileCopyrightText: 2020 Tobias Fella <fella@posteo.de>
- SPDX-FileCopyrightText: 2020 Bhushan Shah <bshah@kde.org>
breeze:
- name: Breeze Light
  url: /img/post-2020-11/breeze.png
- name: Breeze Dark
  url: /img/post-2020-11/dark-breeze.png
created_at: 2020-12-16 14:00:00 UTC
date: "2020-12-16T00:00:00Z"
dialer-screenshots:
- name: Call History
  url: /img/post-2020-11/dialer1.png
- name: Contacts
  url: /img/post-2020-11/dialer2.png
- name: Dialer
  url: /img/post-2020-11/dialer3.png
- name: Call Page
  url: /img/post-2020-11/dialer4.png
cdnJsFiles:
- https://cdn.kde.org/aether-devel/version/kde-org/applications.3e16ae06.js
keyboard-themes:
- name: Breeze Light
  url: /img/post-2020-11/keyboard-light.png
- name: Breeze Dark
  url: /img/post-2020-11/keyboard-dark.png
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
title: Plasma Mobile is finishing 2020 strong with new updates
---

The Plasma Mobile team is happy to present the Plasma Mobile updates from November and December 2020.

## Lock screen

Thanks to Marco Martin, notifications on the lock screen now have actions support. Clicking them requires the user to login before the action is executed.

Thanks to Devin Lin, the keypad now follows the system's color scheme.

![Keypad theme](/img/post-2020-11/keypad-theme.png)

## Shell

Devin added a night color toggle to the top panel. Marco fixed the bug that was stopping the notification history from showing up in the top drop-down panel.

## Virtual Keyboard

Devin worked on the color schemes for the Maliit virtual keyboard based on [initial mockups by manueljlin](https://phabricator.kde.org/T12717).

{{< screenshots name="keyboard-themes" >}}

## Settings

It is now possible to [set a new password in the Settings app](https://invent.kde.org/plasma-mobile/plasma-settings/-/merge_requests/65).

The Wi-Fi settings now use a dialog when asking for a password, improving usability.

## New QtQuick Controls 2 Theme

Noah Davis created a new theme for Plasma Mobile applications. This theme implements our new design goals and, at the same time, provides a faster startup time and a smaller RAM usage.

For example, Index, Plasma Mobile's file manager, now starts 25% faster.

RAM usage has also been decreased, with Kirigami Gallery now using 177MBs of RAM with all its pages open, compared to the 253MBs with the previous theme.

During this work, we also found a bottleneck in the Plasma Desktop theme that we are working to address.

The new theme can be found on [our Gitlab instance](https://invent.kde.org/plasma/qqc2-breeze-style) and can be used by any QtQuick application.

{{< screenshots name="breeze" >}}

## Dialer

The dialer application received a facelift by Devin. There are now placeholder icons for empty lists and a new consistently-sized bottom bar.

The keypad has also been revamped with the removal of animation delays in keypresses and some spacing tweaks.

The call page interface has been redone and now uses consistent button sizing and avatar shadows.

{{< screenshots name="dialer-screenshots" >}}

## Angelfish

Jonah Brüchert added a long-awaited feature to Angelfish: an ad blocker!

It uses the Rust-based implementation from Brave: [plasma-mobile/plasma-angelfish!97](https://invent.kde.org/plasma-mobile/plasma-angelfish/-/merge_requests/97).

Jonah also added support for notifications ([plasma-mobile/plasma-angelfish!99](https://invent.kde.org/plasma-mobile/plasma-angelfish/-/merge_requests/99)) and a download manager ([plasma-mobile/plasma-angelfish!98](https://invent.kde.org/plasma-mobile/plasma-angelfish/-/merge_requests/98)).

With Qt 5.15.2, Angelfish has also become much more stable and doesn't crash when resizing the view anymore.

## KClock

The sidebar of KClock now features a header that is consistent with other Kirigami applications. [plasma-mobile/kclock!54](https://invent.kde.org/plasma-mobile/kclock/-/merge_requests/54)

![KClock header](/img/post-2020-11/kclock-header.png)

## Kalk

Han Young redesigned the layout of the calculator application, Kalk, which now uses a sidebar rather than a bottom bar for switching between views. He also added performance improvements to the unit conversion page.

Devin Lin redesigned the look of the application, especially the calculation page and keypad style.

![Kalk keyboard layout](/img/post-2020-11/kalk.png)

## NeoChat

NeoChat, a Matrix chat client, successfully passed KDE Review, our internal review process for new applications. The newest version of NeoChat allows you to send invitations and accept them, remembers the last room you were in, and opens it again when you open the app. We also improved the read markers, which are now sent correctly, and added a small bar that indicates your last read message.

The handling of images has also been significantly improved and you can drag and drop an image to add it as an attachment. You can also use the standard Ctrl+V shortcut if you have an image in your clipboard. NeoChat now contains a small image editor that lets you crop, rotate, and mirror your images. We also plan on adding some filters in the future. It leverages the KQuickImageEditor library for this which will be useful for more Plasma Mobile applications.

<div class="embed-responsive embed-responsive-16by9">
  <video src="/img/post-2020-11/neochat-2020-12-16_10.35.35.webm" autoplay="true" muted="true" loop="true" />
</div>

The design of NeoChat also improved a lot. The room list is less cluttered, the room sidebar automatically opens when the window is large enough and looks significantly better. Thanks to manueljlin for providing some great mockups!

We also fixed tons of small bugs and made some improvements to the user experience.

![NeoChat sidebar](/img/post-2020-11/neochat-drawer.png)

To get an overview of all bugs fixed and features added, take a look at the [closed issues page](https://invent.kde.org/network/neochat/-/issues?scope=all&utf8=%E2%9C%93&state=closed).

## Okular Mobile

The convergent version of Okular now has a better sidebar that opens automatically on a wider screen and with better spacing.

![Okular Mobile on the desktop](/img/post-2020-11/okular-drawer.png)

When opening Okular, you are presented with a small placeholder message and a button allowing you to open a document.

![Okular placeholder message](/img/post-2020-11/okular-placeholder.png)

## Want to help?

This post shows what a difference new contributors can make. Do you want to be part of our team as well? We prepared [a few ideas for tasks new contributors](https://invent.kde.org/dashboard/issues/?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Mobile&label_name[]=Junior%20Job) can work on. Most coding tasks require some QML knowledge and [KDAB's](https://www.kdab.com/) [QML introduction video series](https://youtu.be/JxyTkXLbcV4) is a great resource for that!

You can join us on various communication channels using the links at [https://www.plasma-mobile.org/join/](https://www.plasma-mobile.org/join/).
