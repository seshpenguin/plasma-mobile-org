---
author: Plasma Mobile team
created_at: 2020-11-12 09:25:00 UTC
date: "2020-11-12T00:00:00Z"
description: The Plasma Mobile team is happy to release the Plasma Mobile updates
  carried out during the month of October. This month's updates include various improvements
  and bugfixes.
image: /img/post-2020-10/post-2020-10-rattlesnake.png
neochat-screenshots:
- name: Room list
  url: /img/post-2020-10/post-2020-10-neochat.png
- name: Timeline
  url: /img/post-2020-10/post-2020-10-neochat-timeline.png
- name: Explore rooms page
  url: /img/post-2020-10/post-2020-10-neochat-explore.png
- name: Emoji inputs
  url: /img/post-2020-10/post-2020-10-neochat-emoji.png
- name: Account management
  url: /img/post-2020-10/post-2020-10-neochat-account.png
rattlesnake-screenshots:
- name: Rattlesnake on a phone
  url: /img/post-2020-10/post-2020-10-rattlesnake.png
- name: Rattlesnake on a desktop
  url: /img/post-2020-10/post-2020-10-rattlesnake-desktop.png
title: 'Plasma Mobile update: October 2020'
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
---

The Plasma Mobile team is happy to share what has been going on during the month of October.

## Shell Improvements

### Task panel

Marco Martin added support for task switcher thumbnails!

<div class="embed-responsive embed-responsive-16by9 mx-auto" style="max-width: 800px">
<video width="480" height="320" controls="controls">
  <source src="/img/post-2020-10/task-switcher-thumbnails.mp4" type="video/mp4">
</video>
</div>

{{< section app_img="/img/post-2020-10/task-pannel.jpg" app_img_shadow="false" app_img_alt="Task Panel" direction="right" >}}
Devin Lin has tweaked the task panel look with some cute icons and fine-tuned sizing.
{{< /section >}}

### Lockscreen

Devin Lin has worked on fixing some lockscreen behaviors. The lockscreen now prevents entering the password during a grace period between password attempts.

The lockscreen keypad now also returns to its initial position when you lift your finger from drags that do not slide enough to open or close the keypad.

{{< section app_name="New SIM PIN Screen" app_img="/img/post-2020-10/sim-pim-screen.jpg" app_img_shadow="true" app_img_alt="Plasma Mobile reworked SIM PIN screent" >}}

Tobias has completely reworked the SIM PIN screen to be visually aligned with the lockscreen.

{{< /section >}}

## Other improvements

* Bhushan made screenshots work correctly [plasma/kwin!415](https://invent.kde.org/plasma/kwin/-/merge_requests/415).
* Nicolas added a quick settings item for toggling Bluetooth [plasma/plasma-phone-components!96](https://invent.kde.org/plasma/plasma-phone-components/-/merge_requests/96).
* Han Young has made the torch quick setting item work on the Pinephone [plasma/plasma-phone-components!105](https://invent.kde.org/plasma/plasma-phone-components/-/merge_requests/105)

## New applications

## Rattlesnake

Mathis created a new application called Rattlesnake. It is a metronome app for musicians and works on mobile and desktop devices.

{{< screenshots name="rattlesnake-screenshots" >}}

## NeoChat

Tobias and Carl worked on NeoChat, a Matrix client. It's a fork of [Spectral](https://gitlab.com/spectral-im/spectral/) and based on the [libQuotient](https://github.com/quotient-im/libQuotient/) library.

{{< screenshots name="neochat-screenshots" >}}

Anyone interested in NeoChat development can join our Matrix room [#neochat:kde.org](https://webchat.kde.org/#/room/#neochat:kde.org). There is still a bunch of work left to do.

## Applications

{{< section app_name="KClock" app_img="/img/post-2020-10/kclock-bottom-panel.gif" app_img_shadow="true" app_img_alt="KWeather Animations" direction="right" >}}

KClock had its v0.2 release! Its daemon's DBus API got a bunch of fixes.

Devin Lin and Han Young have worked on a new bottom panel that now replaces the sidebar when KClock is in portrait mode (as it will be most of the time on mobile).

We have also added a new Season of KDE idea, as we are looking for someone to make a few ringtones and alarm sounds.

{{< /section >}}

{{< section app_name="KWeather" app_img="/img/post-2020-10/kweather-anim.gif" app_img_shadow="true" app_img_alt="KWeather Animations" >}}

Devin Lin and Han Young have continued to work on KWeather, polishing the application for cross-platform usage.

The new dynamic mode now has animations for every weather pattern and has been improved quite a bit. Work will now focus on improving performance on devices like the Pinephone as it is currently very slow.

KWeather now has a much better desktop layout.

![KWeather setup screen](/img/post-2020-10/kweather-desktop.png)

{{< /section >}}

{{< section app_img="/img/post-2020-10/kweather-setup.gif" app_img_shadow="true" app_img_alt="KWeather Setup" >}}

They also added a new setup wizard that allows users to select between using a flat or dynamic view mode. On the Pinephone, it is currently heavily recommended to use flat mode, as the dynamic mode is not optimized performance-wise.

Work is now underway to separate out the weather backend into its own library. This will allow for plasmoids and a weather daemons (for weather alert notifications) to access the weather information without the need to  have the application running in the background.

Did you know KWeather also works on Android? Get it and try it from here: [binary-factory.kde.org/view/Android/job/KWeather_android/](https://binary-factory.kde.org/view/Android/job/KWeather_android/)

{{< /section >}}

### Spacebar

Jonah and Bhushan completely refactored Spacebar, the SMS/MMS application for Plasma Mobile. To avoid messages getting lost then the app isn't active there is now a daemon running in the background. A lot of work went into optimization, stability and user interface improvements.

### Dialer

The dialer now shows the correct call history.

### Plasma Settings

A new settings module allows configuring the Maliit virtual keyboard.

### Alligator

We're currently working on getting Alligator, our RSS feed reader, through KDEReview, which is our internal process to ensure the quality of new projects. This is an important step in getting a released version of the applications.

### QMLKonsole

Marco moved the drawer handles in the footer in QMLKonsole and fixed a bug that made the keyboard close when a terminal extra button (e.g tab, esc) was pressed.

Jonah made QMLKonsole use a native scrollbar.

### Calindori

Our calendar now supports external calendar files. You can use a calendar that has been created and maintained by a different application from within Calindori. For example, if you have set-up calendar file synchronization with [vdirsyncer](https://github.com/pimutils/vdirsyncer), navigate to and click on *Calendar Management > External > Add*, and you can use a calendar that's also used by other devices.

Calindori now uses Plasma's infrastructure for background wakeups to deliver alarms. This improves performance and battery lifetime.
    
User interface features and convergence bits have also been added. In "All Tasks", you can opt to display or hide the completed tasks, while on desktop, when a high resolution monitor is available, the month page is properly scaled.

![Calindori on desktop](/img/post-2020-10/calindori-desktop.png)

### Qrca

Nicolas redesigned the result sheet in Qrca, the Plasma Mobile QR code scanner.

## Neon images

Bhushan migrated the image to Ubuntu Focal (20.04).

## Want to help?

This post shows what a difference new contributors can make. Do you want to be part of our team as well? We prepared [a few ideas for tasks new contributors](https://invent.kde.org/dashboard/issues/?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Mobile&label_name[]=Junior%20Job) can work on. Most coding tasks require some QML knowledge. [KDAB's](https://www.kdab.com/) [QML introduction video series](https://youtu.be/JxyTkXLbcV4) is a great resource for that!

You can join us on various communication channels using the links at [https://www.plasma-mobile.org/join/](https://www.plasma-mobile.org/join/).
