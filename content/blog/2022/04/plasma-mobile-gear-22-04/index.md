---
title: "Plasma Mobile Gear 22.04 is Out"
subtitle: "User interface improvements, callaudiod support and several features"
SPDX-License-Identifier: CC-BY-4.0
date: 2022-04-26
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2022 Bhushan Shah <bshah@kde.org>
- SPDX-FileCopyrightText: 2022 Devin Lin <devin@kde.org>
- SPDX-FileCopyrightText: 2022 Paul Brown <paul.brown@kde.org>
- SPDX-FileCopyrightText: 2022 Aniqa Khokhar <aniqa.khokhar@kde.org>
- SPDX-FileCopyrightText: 2022 Carl Schwan <carl@carlschwan.eu>
- SPDX-FileCopyrightText: 2022 Michael Lang <criticaltemp@protonmail.com>
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
shell:
- name: Homescreen
  url: homescreen.png
- name: App drawer
  url: homescreen2.png
- name: Action drawer
  url: actiondrawer.png
- name: Action drawer in pinned mode
  url: actiondrawer2.png
- name: Task switcher
  url: taskswitcher.png
- name: Website
  url: website.png

tokodon:
  - name: Tokodon profile page on desktop
    url: tokodon-profile-desktop.png
  - name: Tokodon profile page on mobile
    url: tokodon-profile-mobile.png

talk:
  - name: Talk on mobile
    url: nextcloud-talk-mobile.png
  - name: Talk on desktop
    url: nextcloud-talk.png

spacebar:
  - name: Spacebar notification settings
    url: spacebar-notification-settings.png
  - name: Spacebar contact selection
    url: spacebar-contact-selection.png
---

The Plasma Mobile team is happy to announce the release of Plasma Mobile's updates for March-April 2022.

## Website Overhaul

Devin worked on the [website](https://plasma-mobile.org) and updated the homepage design, site pages, content style, as well as the [screenshots page](https://plasma-mobile.org/screenshots/). He also created some [developer documentation pages](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home), useful for helping onboard new contributors to the Plasma stack and tools.

<img src="konqi-calling.png" width=300px/>

## Shell

Devin continued working on fixing issues and incremental improvements on the shell for the upcoming 5.25 release.

{{< screenshots name="shell" >}}

### Task Switcher

The double animation that occurs when activating and minimizing applications was fixed, and Yari added a feature for the task switcher that lets you sort tasks by when they were opened, rather than alphabetically.

### Navigation and Task Panels

The bug which sometimes stopped the task panel from taking the full width of the screen due to scaling changes was fixed. Meanwhile, the navigation panel is now always opaque when the keyboard is open, and the shell windows no longer cause the panels to go opaque when still on the home screen.

### Action Drawer

The action drawer can now be opened from the lock-screen with restricted permissions, and now closes when you tap in an empty space that is not part of the drawer.

A new feature is that you can reorder *quick settings* in the action drawer in the "Shell" settings module, and more reactive *quick setting* press animations were added to provide more contrast than just the colour change.

### Homescreen

You can now switch homescreens. However, new homescreen types need to be added and documented. The eventual goal (in the far, far future) is to allow for 3rd-party homescreens to be distributed on the KDE store.

A grow/shrink animation was added to make app icons feel more reactive to user interaction, and the text in applications was converted to bold to improve readability.

In the bugfix department, the bug that stopped users from being able to interact with the app drawer in empty spaces was fixed.

### Media Player

The media player now supports concurrent streams, so the simultaneous playback from several applications can all be controlled at the same time.

## Settings

Devin updated the design of the cellular networks settings module, and fixed visual issues with the APN page.

<img src="apn.png" alt="Plasma Mobile Mobile data APN settings" width=200px/>

## Plasma Dialer

Bhushan ported plasma-dialer to use the [callaudiod](https://gitlab.com/mobian1/callaudiod) daemon from the Mobian project. This allows us to remove the custom call audio handling code, and will allow us to share the same code across multiple devices and distributions.

## Clock

Devin fixed an issue that caused the `kclockd` daemon to generate excessive logs when on a non-Plasma system, and support for FlatPak background portals was added so the daemon can be autostarted in sandboxes.

## Weather

Stuart fixed an issue where *mmHg* units were being shown as *hPa*.

## Tokodon

[Tokodon](https://apps.kde.org/tokodon/) is a client for Mastodon, the open source and decentralized micro-blogging platform.

Jeremy Winter and Carl Schwan worked on the user profiler viewer, and it now displays all the information available from the Mastodon API. It is also now possible to interact with accounts, allowing you to follow, block, and mute other Mastodon users.

Carl also worked on improving the desktop interface, moving the account selection to a combo box, and moving the different views to the right sidebar from where they were originally located in a tab bar.

Aside from that, Jeremy and Carl fixed a few minor bugs.

{{< screenshots name="tokodon" >}}

## Kalendar

[Kalendar](https://apps.kde.org/kalendar/) is now part of the KDE Gear release schedule and was released with KDE Gear 22.04. This release includes many bug fixes by Claudio Cambra, a lot of cleaning up by Laurent Montel and various dependency clean ups by Volker Krause.

## Nextcloud Talk

Carl made some progress on his Kirigami [Nextcloud Talk](https://github.com/CarlSchwan/talk-desktop) client by implementing a larger part of the room API. Note however that this application should still be considered in alpha state and we do not recommend it to be used in production.

{{< screenshots name="talk" >}}

## Spacebar

Devin Lin made some UI improvements to the header toolbar and app navigation, while Michael Lang improved the *Add Attachment* layout by making the image preview sizes larger and the *Remove* buttons easier to press. He also added notification setting options to hide/show the sender and message content on the lockscreen, and implemented support for choosing a phone number for contacts with multiple numbers.

Kevin Kofler added support for making message links clickable so that activating a link actually opens it, and Hank Strange added a keyboard shortcut for sending messages with <kbd>Ctrl</kbd> + <kbd>Enter</kbd>.

{{< screenshots name="spacebar" >}}

## Contributing

Want to help with the development of Plasma Mobile? Take Plasma Mobile for a spin! Check out the [device support for each distribution](https://www.plasma-mobile.org/get/) and find the version which will work on your phone.

Our [documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Issue-Tracking) gives information on how and where to report issues. Also, consider joining our [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org), and let us know what you would like to work on!
