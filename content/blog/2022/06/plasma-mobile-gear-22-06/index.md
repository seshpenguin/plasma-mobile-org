---
title: "Plasma Mobile Gear ⚙ 22.06 is Out"
subtitle: "More Features, Improved Interface"
SPDX-License-Identifier: CC-BY-4.0
date: 2022-06-28
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2022 Bhushan Shah <bshah@kde.org>
- SPDX-FileCopyrightText: 2022 Bart De Vries <bart@mogwai.be>
- SPDX-FileCopyrightText: 2022 Paul Brown <paul.brown@kde.org>
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
audiotube:
- name: Media Player
  url: audiotube-1.png
- name: Most Played Page
  url: audiotube-2.png
kasts:
- name: Kasts revamped mobile player controls in portrait mode
  url: kasts-mobile-player.png
- name: Kasts revamped mobile player controls in landscape mode
  url: kasts-mobile-player-landscape.png
- name: Kasts supporting custom GPodder servers
  url: kasts-custom-gpodder.png
- name: Kasts on desktop
  url: kasts-desktop.png
tokodon:
- name: Desktop Notification
  url: tokodon-desktop-notification.png
- name: Notification view
  url: tokodon-notification-view.png
- name: Cards
  url: tokodon-card.png
---

The Plasma Mobile team is happy to announce the releases of Plasma Mobile's updates for May-June 2022, as well as the release of Plasma Mobile Gear ⚙ 22.06.

## Shell

Plasma 5.25 was released on June 14 and that brought all the improvements developed from February to May 2022 to the shell.

### Task Switcher

Yari changed the task switcher to sort by last app opened, rather than alphabetically.

The bug which showed two app-minimize animations playing when opening the task switcher was fixed.

Aleix ported the task previews to the new KPipewire component.

### Action Drawer 

Yari Worked on several things to improve the action drawer, the component you see at the top of the screen and that allows you to configure thing like WiFi, battery, airplane mode, etc.

Firstly he added a feature that lets the action drawer display pages. This means that the different options in the action drawer can now accomodate more quick settings. He also added a component that allows for scrolling labels. This means that you can scroll though long text displayed in the action drawer and media player and that is wider than the width of the label. Finally, he  worked on adding the ability to pull down the full quick settings panel in one stroke from one corner of the screen (this feature is configurable).

Devin added the capability for the media player in the action drawer to control multiple sources, and Aleix added a screen recording quick setting.

<img src="actiondrawer.png" alt="Action Drawer" width=200px/>

### Navigation Bar

Devin added vibrations when buttons on the navigation bar are tapped. Vibrations can be customized in the settings.

Yari added a swipe left/right gesture from the navigation bar to quickly switch apps.

### Lockscreen

Devin ported the lockscreen to the kscreenlocker v3 interface, laying the groundwork for supporting passwordless logins. He also fixed lockscreen notification actions not being triggered after unlock.

### Homescreen

Devin fixed an issue in which multiple fast sequential flicks did not open the app drawer.

### Breeze Style

Devin worked on improving the performance of shadows on buttons and other controls. They were previously disabled due to performance issues on lower-end devices, but are now fast enough to be enabled.

<img src="breezestyle.png" alt="Breeze Style" width=200px/>

## Weather

Settings dialogs were ported to use consistent Kirigami components.

## Settings

Devin added settings in the shell settings module to use reduced animations. He also added settings that let you turn off task previews to improve performance on lower-end devices, and fixed issues in the display and colors settings module.

## QMLKonsole

Devin reworked the UI for saved commands, moving it from the sidebar to a page and dialog. He also fixed eliding text in the bottom key buttons.

## AudioTube

Devin worked on improving the media player UI, and worked on the application header bar.

{{< screenshots name="audiotube" >}}

## Kasts

Apart from general bugfixing, Bart worked on the following improvements:
- The podcast update routine has been completely overhauled, solving several bugs.  It's now using multithreading, and it will also update existing episode information.
- General improvements were made to make sure that the app fits well on small screens in both portrait and landscape modes.
- Episode images will now be retrieved from id3v2tag if available.
- New episodes will be added to the queue in ascending chronological order (rather than descending) per podcast.
- A switch has been introduced to optionally adjust the time left on an episodes based on the current playback rate.
- The episode details pages now have a context action to go straight to the list of episodes for that podcast.
- It's now possible to sync with custom GPodder servers (i.e. other than the official gpodder.net instance).
- Podcasts can now again be updated even when the sync server is unavailable. This was a bug in previous versions.

{{< screenshots name="kasts" >}}

## Tokodon

Carl work on adding basic support for notifications. Now you can read previous notifications and you can also get native desktop notifications. Aside from that, Carl added initial support for Nextcloud social and implemented social cards to get a preview of links.

{{< screenshots name="tokodon" >}}


## Contributing

Want to help with the development of Plasma Mobile? Take Plasma Mobile for a spin! Check out the [device support for each distribution](https://www.plasma-mobile.org/get/) and find the version which will work on your phone.

Our [documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Issue-Tracking) gives information on how and where to report issues. Also, consider joining our [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org), and let us know what you would like to work on!
