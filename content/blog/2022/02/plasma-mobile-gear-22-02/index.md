---
title: "Plasma Mobile Gear 22.02 is Out"
subtitle: Smoother gestures, improvements to the Dialer, NeoChat, Clock, and more!
SPDX-License-Identifier: CC-BY-4.0
date: 2022-02-09
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2021 Bhushan Shah <bshah@kde.org>
- SPDX-FileCopyrightText: 2021 Alexey Andreev <aa13q@ya.ru>
- SPDX-FileCopyrightText: 2021 Bart De Vries <bart@mogwai.be>
- SPDX-FileCopyrightText: 2021 Carl Schwan <carl@carlschwan.eu>
- SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>
- SPDX-FileCopyrightText: 2021 Michael Lang <criticaltemp@protonmail.com>
- SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
panel:
- name: Quick Settings Panel
  url: panel-1.png
- name: 
  url: panel-2.png
- name: 
  url: panel-3.png
- name: 
  url: panel-4.png
- name: 
  url: panel-5.png
settings:
- name: Widescreen
  url: settings-1.png
- name: Search
  url: settings-2.png
- name: Header style
  url: settings-3.png
clock:
- name: Time page
  url: kclock-1.png
- name: New form style
  url: kclock-2.png
- name: Add location page
  url: kclock-3.png
- name: Alarm ringing dialog
  url: kclock-4.png
plasmatube:
- name: Home page
  url: plasmatube-1.png
- name: Search page
  url: plasmatube-2.png
calindori:
- name: Sidebar
  url: calindori-1.png
- name: Editing calendar dialog
  url: calindori-2.png
neochat:
- name: NeoChat context menu
  url: neochat-share1.png
- name: NeoChat share context menu
  url: neochat-share2.png
- name: NeoChat share context menu desktop
  url: neochat-share3.png
kasts:
- name: Kasts mobile player controls in landscape mode
  url: kasts-mobile-landscape-player.png
---

The Plasma Mobile team is happy to announce the Plasma Mobile updates for January to February 2022.

## Shell

Plasma 5.24 was released on February 8th, with many changes to the Plasma Mobile shell.

The primary shell repository (and corresponding package name for distributions) has been renamed from plasma-phone-components to [plasma-mobile](https://invent.kde.org/plasma/plasma-mobile). Devin Lin did a lot of work on the shell for this release.

#### Status Bar and Quick Settings Panel

The *Settings* panel has gone through a re-design to improve its looks, and gained new media and notifications widgets. We also implemented a new landscape format for tablets which we plan to improve moving forward.

We carried out a comprehensive clean up of the code, and it now uses a much more reliable method for detecting flicks. This should provide a smoother gesture experience.

{{< screenshots name="panel" >}}

#### Task Switcher & Navigation Panel

The task switcher was rewritten to use a single row of thumbnails with gesture support. We fixed the bugs in the navigation panel that sometimes caused it to gray out, and in the showing of thumbnails in the task switcher.

We explored the possibility of having full gesture navigation (without the navigation panel), but this is not available in the 5.24 release.

<div class="ratio ratio-16x9">
<video src="gestures.mp4" controls autoplay=true muted></video>
</div>

#### Homescreen

The *Homescreen* received a lot of fixes and optimizations in this update. *KRunner* now pops up on the Homescreen when performing a swipe down gesture, allowing for easier searching. The app drawer open/close gesture was rewritten to fix stutters and make flicks smoother.

Plasmoids and apps placed on the Homescreen also received fixes for when they are placed and removed.

The startup indicator when launching an app and the task switcher now reuse the Homescreen window rather than creating new windows. This greatly improves the smoothness of animations on the PinePhone.

#### Other

Aleix Pol implemented mobile designs for XDG portal dialogs.


## Settings

Devin added a new search feature, as well as a widescreen mode for tablets. He also changed the header style to show a back button rather than breadcrumbs. With this improvement, the system module names don't go off the screen.

{{< screenshots name="settings" >}}

## Clock

Devin reworked the backend daemon to better support custom ringtones for alarms. He also reworked the UX of list editing and forms in the application. In-app dialogs were also added to control ringing alarms and timers, as opposed to only being able to do so in notifications.

{{< screenshots name="clock" >}}

## Calindori

Devin took over as caretaker for *Calindori*, previously unmaintained, and started work on improving the application UX.

{{< screenshots name="calindori" >}}

## QMLKonsole

Devin fixed the <kbd>Ctrl</kbd> and <kbd>Alt</kbd> buttons that had not been working, as well as some issues with the physical keyboard focus.

## Recorder

Devin fixed dialogs that were immediately closing when opened, and some visual issues when new recordings were being added.

## PlasmaTube

Devin reworked application navigation to use a bottom toolbar, as well as back-button based headers.

{{< screenshots name="plasmatube" >}}

## Plasma Dialer

Similar to other applications, Devin updated the sidebar button style to follow Plasma style and improved the page navigation. He also added settings and  *About* pages.

Alexey added a prompt dialog to clear the history of calls. He also fixed regressions and restored audio profiles switching during a call and automatic return from a call status page after a call. He also added support for choosing phone numbers via the *Contacts* menu when there are several numbers for the same contact. Another of Alexey's contribution is the logic (based on initial work carried out by Nicolas Fella) that pauses active media players on the device during incoming calls.

## Kasts

Apart from bugfixing, Bart optimized the player controls for landscape orientation on mobile.

{{< screenshots name="kasts" >}}

## Angelfish

Felipe Kinoshita added a desktop UI to Angelfish which already has almost the same feature set of the mobile UI. In the future we will try to bring all features to the desktop version, and share even more code than we do now. As a result of this effort, the settings were ported to `Kirigami.CategorizedSettings`, so they can appear in an additional window on the desktop, while being shown as a page on mobile devices.

In addition to that, Jonah worked on some fixes for the *AdBlock* filter list updater.

## NeoChat

Fushan Wen added support for minimizing the application to the system tray on startup, and Carl Schwan improved the Internet connectivity check. It is now using the connectivity to the server instead of using the global network connection. He also added support for sharing files directly from NeoChat with other applications and online services, such as Nextcloud, Imgur, etc.

Tobias Fella implemented a feature that lets users add labels to accounts. This makes it easier to use NeoChat with multiple accounts.

The NeoChat team also worked on reducing many of the open tickets by fixing minor bugs, closing duplicate tickets and implementing many small features.

{{< screenshots name="neochat" >}}

## Tokodon 

Carl fixed uploading files and cleaned up the code. Devin fixed the mobile sidebar handle icon.

## Contributing

Want to help with the development of Plasma Mobile?

If you would like to take Plasma Mobile for a spin, read this page to see device support for each distribution: https://www.plasma-mobile.org/get/

We now have a general issue tracker, where you can report any issues you encounter here: https://invent.kde.org/teams/plasma-mobile/issues/-/issues

Also, consider joining our [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org), and let us know what you would like to work on!
