---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Communauté
---
Si vous souhaitez contribuer à un incroyable logiciel pour périphériques mobiles, [rejoignez nous - il y aura toujours du travail pour vous]((https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) !

<img src="/img/konqi/konqi-contribute.png" width=200px/>

----

Groupes et canaux de la communauté de Plasma Mobile :

### Canaux spécifiques de Plasma Mobile :

* [![](/img/matrix.svg)Matrix (la plus active)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Liste de discussions de Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canaux concernant les projets relatifs à Plasma Mobile :

* [![](/img/mail.svg)Liste de discussion pour le développement de Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
