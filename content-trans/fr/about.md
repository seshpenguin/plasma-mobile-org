---
menu:
  main:
    parent: project
    weight: 2
title: À propos
---
Plasma Mobile est une interface utilisateur et un éco-système « Open source » visant les périphériques mobiles, construit à partir de la pile **KDE Plasma**.

Une approche pragmatique est suivie, orientée vers les logiciels sans se soucier des boîtes à outils, donnant au utilisateurs, le pouvoir de sélectionner quels logiciels qu'ils désirent pour leur périphérique.

Plasma Mobile a pour objectif de contribuer et d'implémenter des standards ouverts. Il est développé selon un processus transparent, ouvert à toute personne souhaitant y participer.

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## Puis-je l'utiliser ?

Oui ! Plasma Mobile est actuellement préinstallé sur le périphérique « Pinephone » en utilisant la distribution Manjaro ARM. Il y a aussi divers autres distributions pour ce périphérique, incluant Plasma Mobile.

Il y a aussi la distribution PostmarketOS, une distribution Linux reposant sur Alpine, avec la prise en charge de bien plus de périphériques. Elle propose Plasma Mobile comme un interface disponible pour les périphérique qu'elle prend en charge. Vous pouvez voir la liste des périphériques pris en charge [ici](https://wiki.postmarketos.org/wiki/Devices). Mais, pour tout périphérique hors des catégories principales et de la communauté, cela peut varier.

L'interface utilise KWin au dessus de Wayland. Elle est maintenant plutôt stable, malgré quelques défauts autour des bords dans quelques zones. Un sous-ensemble des fonctionnalités standards de KDE Plasma sont disponibles, y compris des composants graphiques et des activités, les deux étant intégrés dans l'interface utilisateur de Plasma Mobile. Ceci le rend possible pour utiliser et développer pour Plasma Mobile sur votre ordinateur de bureau / portable.

## Que peut-il faire ?

Il y a encore que peu d'applications optimisées pour l'interface tactile qui sont maintenant empaquetées dans l'image de Plasma Mobile reposant sur la distribution Manjaro, fournissant une large variété de fonctions basiques.

Ceux-ci sont en grande partie développés avec Kirigami, l'environnement de développement pour interface de KDE, permettant des interfaces utilisateur, fonctionnant très bien avec un environnement tactile.

Vous pouvez trouver une liste de ces applications sur [plasma-mobile.org homepage](https://plasma-mobile.org).

## Où puis-je trouver...

Le code de divers composants de Plasma Mobile peut être trouvé [invent.kde.org](https://invent.kde.org/plasma-mobile).

Veuillez lire la [documentation des contributeurs](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) pour plus de détails sur la pile Plasma et sur comment les choses fonctionnent ensemble.

Vous pouvez aussi poser vos questions sur les [groupes et canaux de la communauté de Plasma Mobile](/join).
