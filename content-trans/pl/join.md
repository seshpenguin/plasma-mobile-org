---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Społeczność
---
Jeśli chciałbyś współtworzyć świetne darmowe oprogramowanie na urządzenia przenośne, [dołącz do nas - zawsze znajdziemy dla ciebie jakieś zadanie] (https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

----

Grupy i kanały społeczności Przenośnej Plazmy:

### Kanały przeznaczone dla Przenośnej Plazmy:

* [![](/img/matrix.svg)Matrix (największy ruch)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Lista rozmów nt. Przenośnej Plazmy](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Kanały związane z Przenośną Plazmą:

* [![](/img/mail.svg)Lista rozmów nt. rozwoju Plazmy](https://mail.kde.org/mailman/listinfo/plasma-devel)
