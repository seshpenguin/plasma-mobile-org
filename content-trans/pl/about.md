---
menu:
  main:
    parent: project
    weight: 2
title: O...
---
Przenośna Plazma to otwarto źródłowy interfejs użytkownika i ekosystem przeznaczony dla urządzeń urządzeń mobilnych, zbudowany na stosie **KDE Plazmy**.

Przyjęto pragmatyczne podejście, które uwzględnia oprogramowanie  niezależnie od zestawu narzędzi, dając użytkownikom możliwość wyboru  oprogramowania, którego chcą używać na swoim urządzeniu.

Przenośna Plazma ma na celu wspieranie i wdrażanie otwartych standardów  i jest rozwijana w przejrzystym procesie, w którym każdy może wziąć udział.

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## Czy mogę tego używać?

Tak! Przenośna Plazma jest domyślnie obecna na PinePhone jako Manjaro ARM. Są też różne inne dystrybucje dla PinePhone z Przenośną Plazmą.

Istnieje również postmarketOS, dystrybucja oparta na Alpine Linux, która obsługuje o wiele więcej urządzeń i oferuje Przenośną Plazmę jako dostępny interfejs dla obsługiwanych urządzeń. Możesz zobaczyć listę obsługiwanych urządzeń [tutaj](https://wiki.postmarketos.org/wiki/Devices), ale na urządzeniach spoza kategorii głównych i społecznościowych może być różnie.

Interfejs wykorzystuje KWin na Waylandzie i jest teraz ogólnie stabilny, choć w niektórych obszarach jest nieco surowy. Dostępny jest zestaw normalnych funkcji KDE Plazma, w tym widżety i aktywności, które są połączone z interfejsem Przenośnej Plazmy. Dzięki temu możliwe jest używanie i tworzenie aplikacji dla Przenośnej Plazmy na komputerze stacjonarnym/laptopie.

## Co potrafi?

Do obrazu Przenośnej Plazmy opartego na Manjaro dołączono sporo aplikacji przystosowanych do obsługi dotykowej, które umożliwiają korzystanie z szerokiego zakresu podstawowych funkcji.

Są one w większości zbudowane przy użyciu Kirigami, szkieletu interfejsu KDE umożliwiającego tworzenie spójnych interfejsów użytkownika, które bardzo dobrze sprawdzają się w środowisku tylko dotykowym.

Listę tych aplikacji można znaleźć na stronie [głównej plasma-mobile.org].(https://plasma-mobile.org).

## Gdzie znajdę...

Kod dla różnych składników Przenośnej Plazmy można znaleźć na stronie  [invent.kde. org](https://invent.kde.org/plasma-mobile).

Przeczytaj [dokumentację współtwórców na] (https://invent.kde.org/plasma/plasma-mobile/-/wikis/home), aby uzyskać więcej szczegółów na temat stosu Plasma i tego, jak wszystko do siebie pasuje.

Możesz także zadawać pytania na[kanałach i grupach społeczności Przenośnej Plazmy](/join).
