---
layout: get-involved
menu:
  main:
    name: 安装
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: 提供 Plasma Mobile 的发行版
---
下面列出了提供 Plasma Mobile 的发行版。

请在各发行版的网站查看您的设备的支持情况。

## 移动设备

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM 是支持 ARM 设备的 Manjaro 发行版。它的安装镜像基于 Arch Linux ARM 构建，配套有 Manjaro 的工具、主题和基础软件体系。

[网站](https://manjaro.org) | [论坛](https://forum.manjaro.org/c/arm/)

#### 下载：

* [最新稳定版 (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [开发测试版 (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### 安装

如果您使用的是 PinePhone，请在 [Pine64 百科网站](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions) 查找相关信息。

----

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) 是一款面向触摸优化、预先配置的 Alpine Linux 发行版，可以安装到安卓智能手机和其他移动设备。请[查看设备列表](https://wiki.postmarketos.org/wiki/Devices)确认您的设备的支持情况。

对于那些未能提供预构建镜像的设备，您必须使用 `pmbootstrap` 工具手动刷入该系统。请根据[安装指南](https://wiki.postmarketos.org/wiki/Installation_guide)进行操作，并记得检查设备对应的百科页面以了解设备的支持情况。

[了解详情](https://postmarketos.org)

#### 下载：

* [支持状态良好的设备](https://postmarketos.org/download/)
* [完整设备列表](https://wiki.postmarketos.org/wiki/Devices)

----

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM 已经由 DanctNIX 社区移植到了 PinePhone 和 PineTab。

#### 下载：

* [正式版本](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

----

### openSUSE

![](/img/openSUSE.svg)

openSUSE，曾用名为 SUSE Linux 和 SuSE Linux 专业版，是一款由 SUSE Linux 公司和其他企业赞助开发的 Linux 发行版。目前 openSUSE 提供了基于 Tumbleweed 的 Plasma Mobile 系统镜像。

#### 下载：

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

----

### Fedora

![](/img/fedora.svg)

Fedora 的支持工作正在进行中，敬请期待！

请加入 Fedora Mobility 的 [matrix 频道](https://matrix.to/#/#mobility:fedoraproject.org)以了解项目进度。

----

## 桌面设备

### postmarketOS

![](/img/pmOS.svg)

postmarketOS 可以在 QEMU 中运行，你可以在电脑上通过虚拟机试用 Plasma Mobile。

详情请见[这篇文章](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64))。在安装过程中选择 Plasma Mobile 作为桌面环境即可。

----

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile 可通过 [AUR](https://aur.archlinux.org/packages/plasma-mobile) 安装。

----

### KDE Neon amd64 ISO 系统镜像

![](/img/neon.svg)

**警告**：此镜像已经不再得到积极维护。

此镜像基于 KDE neon，可以用于在非安卓的 Intel 平板设备、电脑或虚拟机中进行测试。

* [KDE Neon amd64](https://files.kde.org/neon/images/mobile/)
