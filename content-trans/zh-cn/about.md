---
menu:
  main:
    parent: project
    weight: 2
title: 关于
---
Plasma Mobile 是一套面向移动设备的开源用户界面和生态系统，它基于 **KDE Plasma** 程序堆栈构建。

我们认为手机生态系统应该包含尽可能多样的软件，不管它们采用的是何种工具库。这将让用户的选项更加丰富，能够自由选择想用的软件。

Plasma Mobile 项目旨在促进和实现开放标准，并且确保开发过程的开放透明，让任何人都可以参与其中。

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## 我能使用 Plasma Mobile 吗？

当然能。PinePhone 当前已经预装了采用 Plasma Mobile 的 Manjaro ARM 发行版。另外还有多款兼容 PinePhone 的其他发行版，它们均已包含 Plasma Mobile。

postmarketOS 是一款基于 Alpine Linux 的发行版，它支持的设备更多，也提供 Plasma Mobile 作为界面选项。请[查看支持设备列表](https://wiki.postmarketos.org/wiki/Devices)。如果您的设备不在 main 和 community 分类中，那么使用体验可能会受到影响。

Plasma Mobile 的界面采用 KWin 作为显示管理器，运行在 Wayland 模式下。它整体来说已经比较稳定，但某些地方依然存在一些需要改善的问题。KDE Plasma 的一部分功能已经可用，如小工具和活动等已被整合到 Plasma Mobile 的用户界面。您可以在桌面和笔记本环境日常使用 Plasma Mobile 和进行开发。

## Plasma Mobile 能做什么？

基于 Manjaro 的 Plasma Mobile 镜像已经打包了一系列为触控优化的应用，实现了大量必备的功能。

这些应用大多数基于 Kirigami 构建。Kirigami 是 KDE 的桌面和手机环境通用界面程序框架，它对于只能使用触控的环境有良好的兼容性。

您可以在 [Plasma Mobile 主页](https://plasma-mobile.org)查看这些应用的列表。

## 如何参与 Plasma Mobile 项目

Plasma Mobile 的各种组件的源代码被托管在 [invent.kde.org](https://invent.kde.org/plasma-mobile) 网站。

如需了解 Plasma Mobile 程序堆栈的详情和各组件的交互，请阅读 Plasma Mobile 的[贡献者文档](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)。

您还可以在 [Plasma Mobile 社区工作组和聊天频道等处](/join)提问。
