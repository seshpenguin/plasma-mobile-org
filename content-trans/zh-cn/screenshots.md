---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: 主屏幕
  url: /screenshots/screenshot-2022-04-1.png
- name: 应用抽屉 (网格)
  url: /screenshots/screenshot-2022-04-2.png
- name: 应用抽屉 (列表)
  url: /screenshots/screenshot-2022-04-3.png
- name: 操作抽屉 (展开)
  url: /screenshots/screenshot-2022-04-4.png
- name: 操作抽屉 (折叠)
  url: /screenshots/screenshot-2022-04-5.png
- name: 任务切换器
  url: /screenshots/screenshot-2022-04-6.png
- name: 天气
  url: /screenshots/screenshot-2022-04-7.png
- name: 计算器
  url: /screenshots/screenshot-2022-04-8.png
- name: 日历
  url: /screenshots/screenshot-2022-04-9.png
- name: 时钟
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish，网页浏览器
  url: /screenshots/screenshot-2022-04-11.png
- name: Discover：应用商店
  url: /screenshots/screenshot-2022-04-12.png
- name: Plasma Mobile 主屏幕
  url: /screenshots/screenshot-2022-04-13.png
- name: Index，文件管理器
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko：照片库查看器
  url: /screenshots/screenshot-2022-04-15.png
- name: 拨号器
  url: /screenshots/screenshot-2022-04-16.png
- name: 设置
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa：音乐播放器
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels，相机应用
  url: /screenshots/pp_camera.png
- name: 相关硬件
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: 截图
---
以下截图来自正在运行 Plasma Mobile 的 PinePhone 手机。

{{< screenshots name="screenshots" >}}
