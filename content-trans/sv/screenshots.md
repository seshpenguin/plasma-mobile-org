---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: Hemskärm
  url: /screenshots/screenshot-2022-04-1.png
- name: Applikationslåda (rutnät)
  url: /screenshots/screenshot-2022-04-2.png
- name: Applikationslåda (lista)
  url: /screenshots/screenshot-2022-04-3.png
- name: Åtgärdslåda (expanderad)
  url: /screenshots/screenshot-2022-04-4.png
- name: Åtgärdslåda (minimerad)
  url: /screenshots/screenshot-2022-04-5.png
- name: Aktivitetsbyte
  url: /screenshots/screenshot-2022-04-6.png
- name: Väder
  url: /screenshots/screenshot-2022-04-7.png
- name: Räknare
  url: /screenshots/screenshot-2022-04-8.png
- name: Kalender
  url: /screenshots/screenshot-2022-04-9.png
- name: Klocka
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish, en webbläsare
  url: /screenshots/screenshot-2022-04-11.png
- name: Discover, en applikationsbutik
  url: /screenshots/screenshot-2022-04-12.png
- name: Plasma Mobil hemskärm
  url: /screenshots/screenshot-2022-04-13.png
- name: Index, en filhanterare
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko, en fotobiblioteksvisare
  url: /screenshots/screenshot-2022-04-15.png
- name: Uppringning
  url: /screenshots/screenshot-2022-04-16.png
- name: Inställningar
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa, en musikspelare
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels, ett kameraprogram
  url: /screenshots/pp_camera.png
- name: Maskinvaran
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Skärmbilder
---
Följande skärmbilder togs på en PinePhone som kör Plasma Mobil (april 2022).

{{< screenshots name="screenshots" >}}
