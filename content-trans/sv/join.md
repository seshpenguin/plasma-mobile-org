---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Gemenskap
---
Om du vill bidra till den enastående fria programvaran för mobilapparater, [gå med: det finns alltid en uppgift för dig](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home).

<img src="/img/konqi/konqi-contribute.png" width=200px/>

----

Plasma Mobile gemenskapsgrupper och kanaler:

### Plasma Mobil-specifika kanaler:

* [![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma Mobil-relaterade projektkanaler:

* [![](/img/mail.svg)Plasma e-postlista för utvecklare](https://mail.kde.org/mailman/listinfo/plasma-devel)
