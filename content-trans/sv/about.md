---
menu:
  main:
    parent: project
    weight: 2
title: Om
---
Plasma Mobil är ett användargränssnitt och ekosystem med öppen källkod avsett för mobilapparater, byggt på **KDE Plasma**.

Ett pragmatiskt tillvägagångssätt tas som inkluderar programvara oberoende av verktygslåda vilket ger användare möjlighet att välja vilken programvara de vill använda på sina apparater.

Målet med Plasma Mobil är bidra till och implementera öppna standarder, och utvecklas med en transparent process som är öppen för vem som helst att delta i.

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## Kan jag använda det?

Ja! Plasma Mobil är för närvarande förinstallerad på PinePhone med användning av Manjaro ARM. Det finns också diverse andra distributioner tillgängliga för PinePhone med Plasma Mobil.

Det finns också postmarketOS, en distribution baserad på Alpine Linux med stöd för många andra apparater och den erbjuder Plasma Mobil som ett tillgängligt gränssnitt för apparaterna den stöder. Du hittar listan med apparater som stöds [här](https://wiki.postmarketos.org/wiki/Devices), men på apparater utanför huvudkategorin och gemenskapskategorin kan resultatet variera.

Gränssnittet använder Kwin via Wayland och är nu i huvudsak stabilt, även om det har en del vassa kanter på sina ställen. En delmängd av de normala funktionerna i KDE Plasma är tillgängliga, inklusive grafiska komponenter och aktiviteter, som båda är integrerade i Plasma Mobil användargränssnittet. Det gör det möjligt att använda och utveckla för Plasma Mobil på en skrivbordsdator eller bärbar dator.

## Vad kan det göra?

Det finns en hel del applikationer optimerade för pekskärmar som nu levereras tillsammans med Plasma Mobil baserad på Manjaro, vilka tillåter ett stort antal grundfunktioner.

De är i huvudsak byggda med Kirigami, KDE:s gränssnittsramverk som tillåter konvergenta användargränssnitt som fungerar mycket bra i en miljö med bara pekskärm.

Det finns en lista över de här applikationer på [hemsidan plasma-mobile.org](https://plasma-mobile.org).

## Var hittar jag …

Koden för olika komponenter av Plasma Mobil finns på [invent.kde.org](https://invent.kde.org/plasma-mobile).

Läs [bidragsgivarnas dokumentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) för mer information om Plasma och hur allt passar ihop.

Du kan också ställa frågor i [Plasma Mobile gemenskapsgrupper och kanaler](/join).
