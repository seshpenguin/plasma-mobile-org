---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: Homescreen
  url: /screenshots/screenshot-2022-04-1.png
- name: App Drawer (Grid)
  url: /screenshots/screenshot-2022-04-2.png
- name: App Drawer (List)
  url: /screenshots/screenshot-2022-04-3.png
- name: Action Drawer (Expanded)
  url: /screenshots/screenshot-2022-04-4.png
- name: Action Drawer (Minimized)
  url: /screenshots/screenshot-2022-04-5.png
- name: Task Switcher
  url: /screenshots/screenshot-2022-04-6.png
- name: Weather
  url: /screenshots/screenshot-2022-04-7.png
- name: Calculator
  url: /screenshots/screenshot-2022-04-8.png
- name: Calendar
  url: /screenshots/screenshot-2022-04-9.png
- name: Clock
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish, a web browser
  url: /screenshots/screenshot-2022-04-11.png
- name: Discover, an app store
  url: /screenshots/screenshot-2022-04-12.png
- name: Plasma mobile homescreen
  url: /screenshots/screenshot-2022-04-13.png
- name: Index, a file manager
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko, a photo library viewer
  url: /screenshots/screenshot-2022-04-15.png
- name: Dialer
  url: /screenshots/screenshot-2022-04-16.png
- name: Settings
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa, a music player
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels, a camera application
  url: /screenshots/pp_camera.png
- name: The hardware
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Zachytené obrazovky
---
The following screenshots were taken from a Pinephone device running Plasma Mobile (April 2022).

{{< screenshots name="screenshots" >}}
