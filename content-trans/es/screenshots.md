---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: Pantalla de inicio
  url: /screenshots/screenshot-2022-04-1.png
- name: Cajón de aplicaciones (Cuadrícula)
  url: /screenshots/screenshot-2022-04-2.png
- name: Cajón de aplicaciones (lista)
  url: /screenshots/screenshot-2022-04-3.png
- name: Cajón de acción (ampliado)
  url: /screenshots/screenshot-2022-04-4.png
- name: Cajón de acciones (minimizado)
  url: /screenshots/screenshot-2022-04-5.png
- name: Cambiador de aplicaciones
  url: /screenshots/screenshot-2022-04-6.png
- name: El Tiempo
  url: /screenshots/screenshot-2022-04-7.png
- name: Calculadora
  url: /screenshots/screenshot-2022-04-8.png
- name: Calendario
  url: /screenshots/screenshot-2022-04-9.png
- name: Reloj
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish, un navegador web
  url: /screenshots/screenshot-2022-04-11.png
- name: Discover, una tienda de aplicaciones
  url: /screenshots/screenshot-2022-04-12.png
- name: Pantalla inicial de Plasma Mobile
  url: /screenshots/screenshot-2022-04-13.png
- name: Index, un gestor de archivos
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko, un visor de bibliotecas fotográficas
  url: /screenshots/screenshot-2022-04-15.png
- name: Marcador
  url: /screenshots/screenshot-2022-04-16.png
- name: Ajustes
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa, un reproductor de música
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels, una aplicación de cámara
  url: /screenshots/pp_camera.png
- name: El hardware
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Capturas de pantalla
---
Las siguientes capturas de pantalla se han tomado en un dispositivo PinePhone que ejecuta Plasma Mobile (abril 2022).

{{< screenshots name="screenshots" >}}
