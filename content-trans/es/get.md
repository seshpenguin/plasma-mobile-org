---
layout: get-involved
menu:
  main:
    name: Instalación
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Distribuciones que ofrecen Plasma Mobile
---
Listado de distribuciones que ofrecen Plasma Mobile

Por favor, compruebe la información de cada distribución para ver si su dispositivo es compatible.

## Móvil

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM es la distribución Manjaro, pero para dispositivos ARM. Está basada en Arch Linux ARM, combinada con las herramientas de Manjaro, los temas y la infraestructura necesarios para crear imágenes que se puedan instalar en dispositivos ARM.

[Sitio web](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

#### Descargar

* [Última estable (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Compilaciones de desarrolladores (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Instalación

Puede encontrar información general sobre el PinePhone en [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

----

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), es un Alpine Linux preconfigurado y optimizado para pantallas táctiles que se puede instalar en *smartphones* y en otros dispositivos móviles. Consulte la [lista de dispositivos](https://wiki.postmarketos.org/wiki/Devices) para ver el estado de compatibilidad con su dispositivo.

Para los dispositivos que no disponen de imágenes precompiladas, tendrá que «flashearlas» de forma manual usando la utilidad `pmbootstrap`. Siga las instrucciones [aquí](https://wiki.postmarketos.org/wiki/Installation_guide). Asegúrese también de comprobar la página wiki del dispositivo para obtener más información sobre lo que funciona.

[Saber más](https://postmarketos.org)

#### Descargar

* [Dispositivos con buen soporte](https://postmarketos.org/download/)
* [Lista completa de dispositivos](https://wiki.postmarketos.org/wiki/Devices)

----

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM ha sido portado al PinePhone y PineTab por la comunidad DanctNIX.

#### Descargar

* [Lanzamientos](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

----

### openSUSE

![](/img/openSUSE.svg)

openSUSE, anteriormente SUSE Linux y SuSE Linux Professional, es una distribución Linux patrocinada por SUSE Linux GmbH y otras compañías. En la actualidad, openSUSE proporciona compilaciones de Plasma Mobile basadas en Tumbleweed.

#### Descargar

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

----

### Fedora

![](/img/fedora.svg)

Se trata de un trabajo en curso, ¡esté atento!

Únase al [canal matrix](https://matrix.to/#/#mobility:fedoraproject.org) de Fedora Mobility para obtener detalles sobre el progreso.

----

## Dispositivos de escritorio

### postmarketOS

![](/img/pmOS.svg)

postmarketOS se puede ejecutar en QEMU, que es opción adecuada para probar Plasma Mobile en su equipo.

Lea más sobre ello [aquí](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). Durante el proceso de configuración, seleccione Plasma Mobile como entorno de escritorio.

----

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile está disponible en [AUR](https://aur.archlinux.org/packages/plasma-mobile).

----

### Imagen ISO amd64 basada en Neon

![](/img/neon.svg)

**ADVERTENCIA**: No se mantiene de forma activa.

Esta imagen, basada en KDE Neon, se puede probar en tabletas Intel sin Android, PC y máquinas virtuales.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
