---
menu:
  main:
    parent: project
    weight: 2
title: Acerca de
---
Plasma Mobile es una interfaz de usuario de código abierto y un ecosistema dirigido a dispositivos móviles, construido sobre **KDE Plasma**.

Se adopta un enfoque pragmático que incluye el software independientemente del conjunto de herramientas, dando a los usuarios el poder de elegir el software que quieran utilizar en su dispositivo.

Plasma Mobile tiene como objetivo contribuir y aplicar los estándares abiertos, y se desarrollado en un proceso transparente en el que puede participar cualquiera.

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## ¿Puedo usarlo?

Sí. Plasma Mobile viene actualmente preinstalado en el PinePhone usando Manjaro ARM. También hay otras distribuciones para el PinePhone con Plasma Mobile disponibles.

También existe postmarketOS, una distribución basada en Alpine Linux con soporte para muchos más dispositivos y ofrece Plasma Mobile como interfaz disponible para los dispositivos que soporta. Puedes ver la lista de dispositivos soportados [aquí](https://wiki.postmarketos.org/wiki/Devices), pero en cualquier dispositivo fuera de las categoría principal y comunitaria su usabilidad puede variar.

La interfaz utiliza KWin sobre Wayland y ahora es mayormente estable, aunque un poco tosca en algunas áreas. Un subconjunto de las características normales de KDE Plasma están disponibles, incluyendo widgets y actividades, los cuales están integrados en la interfaz de usuario de Plasma Mobile. Esto hace que sea posible utilizar y desarrollar para Plasma Mobile en su escritorio/portátil.

## ¿Qué puedo hacer?

Hay un buen número de aplicaciones optimizadas para el uso táctil que se incluyen con la imagen Plasma Mobile basada en Manjaro, permitiendo una amplia gama de funciones básicas.

La mayoría de ellas están construidas con Kirigami, el framework de interfaz de KDE que permite interfaces convergentes que funcionan muy bien en un entorno sólo táctil.

Un listado de estas aplicaciones se encuentra en [la web plasma-mobile.org](https://plasma-mobile.org).

## ¿Dónde puedo encontrar...?

El código para varios componentes de Plasma Mobile está en [invent.kde.org](https://invent.kde.org/plasma-mobile).

Lea la [documentación de los colaboradores](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) para más detalles sobre el *stack* Plasma y cómo todo encaja.

Para preguntas existen los [grupos y canales de la comunidad de Plasma Mobile](/join).
