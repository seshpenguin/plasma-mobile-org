---
menu:
  main:
    parent: project
    weight: 2
title: Informazioni su
---
Plasma Mobile è un'interfaccia utente e un ecosistema open source destinato ai dispositivi mobili, integrato sulla pila di **KDE Plasma**.

Viene preso un approccio pragmatico, inclusivo per il software indipendentemente dal toolkit, offrendo agli utenti il potere di scegliere il software che vogliono utilizzare sul loro dispositivo.

Plasma Mobile aspira a creare e ad implementare degli standard aperti. È sviluppato in un processo trasparente, che è aperto a chiunque voglia parteciparvi.

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## Posso usarlo?

Sì! Plasma Mobile attualmente è preinstallato sul Pinephone, che usa Manjaro ARM. Ci sono anche altre distribuzioni per PinePhone con Plasma Mobile disponibile.

C'è anche postmarketOS, una distribuzione basata su Alpine Linux col supporto a molti dispositivi, che offre Plasma Mobile come un'interfaccia per i dispositivi che sono supportati. [Qui](https://wiki.postmarketos.org/wiki/Devices) puoi vedere l'elenco dei dispositivi supportati, ma la tua esperienza potrebbe essere diversa con qualsiasi dispositivo al di fuori delle categorie principali e della comunità.

L'interfaccia utilizza KWin su Wayland ed è ora per lo più stabile, anche se un po' grezza in alcune aree attorno ai bordi. È disponibile un sottoinsieme delle normali funzionalità di KDE Plasma, inclusi gli oggetti e le attività, che sono stati integrati entrambi nell'interfaccia di Plasma Mobile. Questo rende possibile possibile utilizzare e sviluppare Plasma Mobile sul tuo computer fisso o portatile.

## Cosa posso fare?

Ci sono alcune app ottimizzate per il tocco che sono ora incorporate nell'immagine di Plasma Mobile basata su Manjaro, che permettono di utilizzare una vasta gamma di funzioni di base.

Sono compilate principalmente con il framework per l'interfaccia di KDE, Kirigami, che permette di sviluppare interfacce convergenti che funzionano molto bene in ambienti solo a tocco.

Puoi trovare un elenco di queste applicazioni sulla [pagina iniziale di plasma-mobile.org](https://plasma-mobile.org).

## Dove posso trovare...

Puoi trovare il codice dei vari componenti di Plasma Mobile in [invent.kde.org](https://invent.kde.org/plasma-mobile).

Leggi la [documentazione dei contributori](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) per maggiori dettagli sulla pila di Plasma e per scoprire come tutto si adatti alla perfezione.

Puoi porre le tue domande nei [gruppi e nei canali della comunità di Plasma Mobile](/join).
