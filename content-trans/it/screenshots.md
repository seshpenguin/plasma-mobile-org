---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: Schermata home
  url: /screenshots/screenshot-2022-04-1.png
- name: App Drawer (Grid)
  url: /screenshots/screenshot-2022-04-2.png
- name: App Drawer (List)
  url: /screenshots/screenshot-2022-04-3.png
- name: Action Drawer (Expanded)
  url: /screenshots/screenshot-2022-04-4.png
- name: Action Drawer (Minimized)
  url: /screenshots/screenshot-2022-04-5.png
- name: Selettore delle finestre
  url: /screenshots/screenshot-2022-04-6.png
- name: Meteo
  url: /screenshots/screenshot-2022-04-7.png
- name: Calcolatrice
  url: /screenshots/screenshot-2022-04-8.png
- name: Calendario
  url: /screenshots/screenshot-2022-04-9.png
- name: Orologio
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish, un browser web
  url: /screenshots/screenshot-2022-04-11.png
- name: Discover, un app store
  url: /screenshots/screenshot-2022-04-12.png
- name: Schermata iniziale di Plasma Mobile
  url: /screenshots/screenshot-2022-04-13.png
- name: Index, un gestore di file
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko, un visore della libreria fotografica
  url: /screenshots/screenshot-2022-04-15.png
- name: Combinatore
  url: /screenshots/screenshot-2022-04-16.png
- name: Impostazioni
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa, un lettore musicale
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels, un'applicazione per fotocamera
  url: /screenshots/pp_camera.png
- name: L'hardware
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Schermate
---
Le seguenti schermate sono state prese da un dispositivo Pinephone con in esecuzione Plasma Mobile (aprile 2022).

{{< screenshots name="screenshots" >}}
