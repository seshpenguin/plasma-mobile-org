---
layout: get-involved
menu:
  main:
    name: Installa
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Distribuzioni che offrono Plasma Mobile
---
Qui sotto sono elencate le distribuzioni che offrono Plasma Mobile

Controlla le informazioni su ogni distribuzione, per vedere se il tuo dispositivo è supportato.

## Cellulare

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM è la distribuzione Manjaro, ma per dispositivi ARM. È basata su Arch Linux ARM, combinata con gli strumenti, i temi e l'infrastruttura di Manjaro per creare delle immagini di installazione per il tuo dispositivo ARM.

[Sito web](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

#### Scarica

* [Ultima versione stabile di (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Build degli sviluppatori (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Installazione

Per il PinePhone puoi trovare delle informazioni generiche nel [wiki di Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

----

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) è un Alpine Linux ottimizzato per il tocco e preconfigurato che può essere installato su smartphone e su altri dispositivi mobili. Visualizza la [lista dei dispositivi](https://wiki.postmarketos.org/wiki/Devices) per vedere il progresso del supporto al tuo dispositivo.

Per i dispositivi che non hanno immagini precostituite, sarà necessario scriverla usando il programma «pmbootstrap». Segui [queste](https://wiki.postmarketos.org/wiki/Installation_guide) istruzioni. Controlla anche la pagina wiki del dispositivo per ulteriori informazioni su cosa funziona.

[Scopri di più](https://postmarketos.org)

#### Scarica

* [Dispositivi ben supportati](https://postmarketos.org/download/)
* [Elenco completo dei dispositivi](https://wiki.postmarketos.org/wiki/Devices)

----

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM è stato convertito per PinePhone e PineTab dalla comunità DanctNIX.

#### Scarica

* [Rilasci](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

----

### openSUSE

![](/img/openSUSE.svg)

openSUSE, in precedenza SUSE Linux e SuSE Linux Professional, è una distribuzione Linux sponsorizzata da SUSE Linux GmbH e da altre aziende. Attualmente openSUSE fornisce build di Plasma Mobile basate su Tumbleweed.

#### Scarica

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

----

### Fedora

![](/img/fedora.svg)

Lavoro in corso, rimani sintonizzato!

Unisciti al [canale matrix](https://matrix.to/#/#mobility:fedoraproject.org) per avere i dettagli sull'avanzamento.

----

## Dispositivi desktop

### postmarketOS

![](/img/pmOS.svg)

postmarketOS può essere eseguito in QEMU, e quindi è un'opzione adatta per provare Plasma Mobile sul tuo computer.

Leggi di più a riguardo [qui](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). Durante il processo di configurazione, seleziona semplicemente Plasma Mobile come ambiente desktop.

----

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile è ora disponibile su [AUR](https://aur.archlinux.org/packages/plasma-mobile).

----

### Immagine ISO basata su Neon amd64

![](/img/neon.svg)

**AVVISO**: non viene mantenuta attivamente!

Questa immagine, basata su neon di KDE, può essere provata su tablet Intel non Android, PC e macchine virtuali.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
