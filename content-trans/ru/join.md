---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Сообщество
---
If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

----

Plasma Mobile community groups and channels:

### Plasma Mobile specific channels:

* [![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma Mobile related project channels:

* [![](/img/mail.svg)Plasma development mailing list](https://mail.kde.org/mailman/listinfo/plasma-devel)
