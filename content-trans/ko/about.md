---
menu:
  main:
    parent: project
    weight: 2
title: 정보
---
Plasma 모바일은 **KDE Plasma** 스택을 사용하는 모바일 장치용 오픈 소스 사용자 인터페이스이자 생태계입니다.

실용적인 접근 방식을 사용하여 툴킷에 관계 없이 소프트웨어를 포용하며, 사용자가 원하는 대로 장치에 소프트웨어를 설치할 수 있습니다.

Plasma 모바일은 공개 표준에 기여하고 구현하는 것을 목표로 하며, 누구나 참여할 수 있는 투명한 과정으로 개발됩니다.

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## 사용할 수 있나요?

네! Plasma 모바일은 PinePhone에 Manjaro ARM 위에 설치되어 있습니다. Plasma 모바일이 탑재된 PinePhone용 다른 배포판도 있습니다.

Alpine 리눅스 기반 배포판인 postmarketOS에서도 사용할 수 있습니다. 더 많은 모바일 장치를 지원하며 지원하는 장치 인터페이스로 Plasma 모바일을 제공합니다. 지원하는 장치 목록은 [여기](https://wiki.postmarketos.org/wiki/Devices)에서 확인할 수 있지만, 주 및 커뮤니티 분류 외에 있는 장치 지원은 안정적이지 않을 수도 있습니다.

인터페이스는 KWin/Wayland를 사용하며 일부 다듬어지지 않은 부분을 제외하면 안정적으로 사용 가능합니다. 위젯과 활동을 포함한 일반 KDE Plasma 기능의 일부분을 사용할 수 있으며, Plasma 모바일 UI에 통합되어 있습니다. 즉 Plasma 모바일을 데스크톱/노트북에서 사용하고 개발할 수 있습니다.

## 무엇을 할 수 있나요?

기본 기능을 수행할 수 있도록 Manjaro 기반 Plasma 모바일 이미지에 일부 터치 최적화된 앱이 포함되어 있습니다.

대부분은 KDE의 인터페이스 프레임워크인 Kirigami를 사용했습니다. 터치 전용 환경에서도 작동하는 통합형 UI를 구현합니다.

[plasma-mobile.org 홈페이지](https://plasma-mobile.org)에서 프로그램 목록을 확인할 수 있습니다.

## 어디에서 찾을 수 있나요?

다양한 Plasma 모바일 구성 요소의 소스 코드는 [invent.kde.org](https://invent.kde.org/plasma-mobile)에서 찾을 수 있습니다.

Plasma 스택에 대한 정보와 모든 것이 작동하는 방식을 알아 보려면 [기여자 문서](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)를 읽어 보십시오.

[Plasma 모바일 커뮤니티 그룹과 채널](/join)에 질문할 수도 있습니다.
