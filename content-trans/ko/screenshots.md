---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: 홈 화면
  url: /screenshots/screenshot-2022-04-1.png
- name: 앱 서랍(격자)
  url: /screenshots/screenshot-2022-04-2.png
- name: 앱 서랍(목록)
  url: /screenshots/screenshot-2022-04-3.png
- name: 동작 서랍(확장됨)
  url: /screenshots/screenshot-2022-04-4.png
- name: 동작 서랍(최소화됨)
  url: /screenshots/screenshot-2022-04-5.png
- name: 작업 전환기
  url: /screenshots/screenshot-2022-04-6.png
- name: 날씨
  url: /screenshots/screenshot-2022-04-7.png
- name: 계산기
  url: /screenshots/screenshot-2022-04-8.png
- name: 캘린더
  url: /screenshots/screenshot-2022-04-9.png
- name: 시계
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish, 웹 브라우저
  url: /screenshots/screenshot-2022-04-11.png
- name: Discover, 앱 스토어
  url: /screenshots/screenshot-2022-04-12.png
- name: Plasma 모바일 홈 화면
  url: /screenshots/screenshot-2022-04-13.png
- name: Index, 파일 관리자
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko, 사진 라이브러리 뷰어
  url: /screenshots/screenshot-2022-04-15.png
- name: 다이얼
  url: /screenshots/screenshot-2022-04-16.png
- name: 설정
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa, 음악 재생기
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels, 카메라 프로그램
  url: /screenshots/pp_camera.png
- name: 하드웨어
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: 스크린샷
---
다음 스크린샷은 PinePhone에서 실행 중인 Plasma 모바일에서 촬영했습니다(2022년 4월).

{{< screenshots name="screenshots" >}}
