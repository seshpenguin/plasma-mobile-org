---
layout: get-involved
menu:
  main:
    name: 설치
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Plasma 모바일이 탑재된 배포판
---
아래는 Plasma 모바일이 탑재된 배포판 목록입니다.

장치 지원 여부는 각각 배포판 문서를 확인하십시오.

## 휴대폰

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM은 Arm 장치용 Manjaro 배포판입니다. Arch Linux ARM을 기반으로 하며, Manjaro에서 제공하는 도구, 테마, 기반을 사용하여 Arm 장치에 설치할 수 있는 이미지를 만듭니다.

[웹사이트](https://manjaro.org) [포럼](https://forum.manjaro.org/c/arm/)

#### 다운로드

* [최종 안정판(PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [개발자 빌드(Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### 설치

PinePhone 사용자라면 [Pine64 위키](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions)에서 정보를 확인할 수 있습니다.

----

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS(pmOS)는 스마트폰과 기타 모바일 장치에 설치할 수 있는 터치에 최적화된 Alpine 리눅스의 파생형입니다. 장치 지원 여부를 확인하려면 [장치 목록](https://wiki.postmarketos.org/wiki/Devices)을 참조하십시오.

미리 빌드된 이미지가 없는 장치를 사용하고 있다면 `pmbootstrap` 유틸리티를 사용하여 수동으로 플래시해야 합니다. [여기](https://wiki.postmarketos.org/wiki/Installation_guide)에 있는 안내 사항을 따르십시오. 진행하기 전에 장치 지원 상태를 장치별 위키 페이지에서 확인하십시오.

[더 알아보기](https://postmarketos.org)

#### 다운로드

* [잘 지원되는 장치](https://postmarketos.org/download/)
* [전체 장치 목록](https://wiki.postmarketos.org/wiki/Devices)

----

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM은 DanctNIX 커뮤니티에 의해서 PinePhone과 PineTab으로 이식되었습니다.

#### 다운로드

* [릴리스](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

----

### openSUSE

![](/img/openSUSE.svg)

openSUSE는 과거에 SUSE Linux, SuSE Linux Professional로도 알려져 있으며, SUSE Linux GmbH 및 기타 회사에서 지원하는 리눅스 배포판입니다. 현재 openSUSE에서는 Tumbleweed 기반 Plasma 모바일 빌드를 제공합니다.

#### 다운로드

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

----

### 페도라

![](/img/fedora.svg)

아직 작업 중이므로 계속 지켜 보세요!

진행 상황 정보는 페도라 Mobility [matrix 채널](https://matrix.to/#/#mobility:fedoraproject.org)에서 확인하십시오.

----

## 데스크톱 장치

### postmarketOS

![](/img/pmOS.svg)

postmarketOS는 QEMU에서 실행시킬 수 있으며, 컴퓨터에서 Plasma 모바일을 체험해 볼 수 있는 좋은 방법입니다.

더 많은 정보는 [여기](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64))에서 확인할 수 있습니다. 설치 과정에서 데스크톱 환경으로 Plasma 모바일을 선택하십시오.

----

### Arch Linux

![](/img/archlinux.png)

Plasma 모바일은 [AUR](https://aur.archlinux.org/packages/plasma-mobile)에서도 사용할 수 있습니다.

----

### Neon 기반 amd64 ISO 이미지

![](/img/neon.svg)

**경고**: 더 이상 활발하게 관리되지 않습니다!

이 이미지는 KDE neon 기반이며 안드로이드를 사용하지 않는 인텔/AMD 태블릿, PC, 가상 머신에서 실행 가능합니다.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
