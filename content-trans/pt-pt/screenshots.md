---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: Ecrã Inicial
  url: /screenshots/screenshot-2022-04-1.png
- name: Área de Aplicações (Grelha)
  url: /screenshots/screenshot-2022-04-2.png
- name: Área de Aplicações (Lista)
  url: /screenshots/screenshot-2022-04-3.png
- name: Área de Acções (Expandida)
  url: /screenshots/screenshot-2022-04-4.png
- name: Área de Acções (Minimizada)
  url: /screenshots/screenshot-2022-04-5.png
- name: Selector de Tarefas
  url: /screenshots/screenshot-2022-04-6.png
- name: Tempo
  url: /screenshots/screenshot-2022-04-7.png
- name: Calculadora
  url: /screenshots/screenshot-2022-04-8.png
- name: Calendário
  url: /screenshots/screenshot-2022-04-9.png
- name: Relógio
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish, um navegador Web
  url: /screenshots/screenshot-2022-04-11.png
- name: Discover, uma loja de aplicações
  url: /screenshots/screenshot-2022-04-12.png
- name: Ecrã inicial do Plasma Mobile
  url: /screenshots/screenshot-2022-04-13.png
- name: Index, um gestor de ficheiros
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko, um visualizador de bibliotecas de fotografias
  url: /screenshots/screenshot-2022-04-15.png
- name: Teclado
  url: /screenshots/screenshot-2022-04-16.png
- name: Configuração
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa, um leitor de música
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels, uma aplicação de câmara fotográfica
  url: /screenshots/pp_camera.png
- name: O 'hardware'
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Imagens
---
As seguintes imagens foram capturadas a partir de um dispositivo Pinephone, a executar o Plasma Mobile (Abril de 2022).

{{< screenshots name="screenshots" >}}
