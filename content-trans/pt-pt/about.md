---
menu:
  main:
    parent: project
    weight: 2
title: Acerca
---
O Plasma Mobile é uma interface e ecossistema de código aberto destinado a dispositivos móveis, assente sobre a plataforma do **Plasma do KDE**.

É tomada uma abordagem pragmática que é inclusiva no que respeita ao 'software', independentemente das ferramentas, que dá aos utilizadores o poder de escolher quais as aplicações que desejam usar no seu dispositivo.

O Plasma Mobile pretende contribuir e implementar normas abertas, estando desenvolvido num processo transparente que está aberto para todos participarem.

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## Posso usá-lo?

Sim! O Plasma Mobile de momento vem pré-instalado no PinePhone, usando o Manjaro ARM. Existem também outras distribuições diversas para o PinePhone com o Plasma Mobile disponível..

Existe também o postmarketOS, uma distribuição baseada no Alpine Linux com suporte para muitos mais dispositivos, fornecendo o Plasma Mobile como uma interface disponível para os dispositivos que suporta. Poderá descobrir a lista de dispositivos suportados [aqui](https://wiki.postmarketos.org/wiki/Devices), mas em qualquer dispositivo fora das categorias principais e da comunidade, a sua experiência pode variar.

A interface está a usar o KWin sobre o Wayland e está na sua maioria a um nível estável, ainda que com algumas arestas por limar em certas áreas. Está disponível um sub-conjunto das funcionalidades normais do Plasma do KDE, incluindo os elementos gráficos e as actividades, estando ambos integrados na interface do Plasma Mobile. Isto torna possível usar e desenvolver para o Plasma Mobile no seu computador pessoal/portátil.

## O que ele pode fazer?

Existem algumas aplicações optimizadas para ecrãs tácteis que estão a ser disponibilizadas com a imagem do Plasma Mobile baseada no Manjaro, permitindo uma grande gama de funções básicas.

Estas são desenvolvidas na sua maioria com o  Kirigami, a plataforma de interfaces do KDE que permite UI's convergentes que funcionam muito bem em ambientes tácteis.

Poderá descobrir uma lista destas aplicações na [página Web do plasma-mobile.org](https://plasma-mobile.org).

## Onde é que posso encontrar…

O código dos vários componentes do Plasma Mobile pode ser encontrado no [invent.kde.org](https://invent.kde.org/plasma-mobile).

Leia a [documentação dos colaboradores](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) para saber mais detalhes sobre a plataforma do Plasma e como tudo se conjuga.

Também poderá efectuar as suas perguntas nos [Grupos e canais da comunidade do Plasma Mobile](/join).
