---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Skupnost
---
Če želite prispevati k čudoviti prosti programski opremi za mobilne naprave, [se nam pridružite - vedno imamo nalogo za vas](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

----

Skupine in kanali skupnosti Plasma Mobile:

### Specifični kanali za Plasma Mobile:

* [![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Kanali, ki se nanašajo Plasma Mobile:

* [![](/img/mail.svg)Plasma development mailing list](https://mail.kde.org/mailman/listinfo/plasma-devel)
