---
menu:
  main:
    parent: project
    weight: 2
title: O projektu
---
Plasma Mobile je odprtokoden uporabniški vmesnik in ekosistem, namenjen mobilnim napravam, zgrajen na skladu **KDE Plasma**.

Sprejet je pragmatični pristop, ki je vključujoč za programsko opremo ne glede na ogrodje, na katerem temelji, kar uporabnikom daje možnost izbire poljubne programske opreme, ki jo želijo uporabiti na svoji napravi.

Cilj plasma Mobile je prispevati in slediti odprtim standardom ter je razvit v preglednem procesu, odprtem za sodelovanje.

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## Ga lahko uporabim?

Da! Plasma Mobile je trenutno prednameščen na PinePhone s sistemom Manjaro ARM. Obstajajo tudi različne druge distribucije za PinePhone, za katere je na voljo Plasma Mobile.

Na voljo je tudi postmarketOS, distribucija na osnovi Alpine Linux s podporo za veliko več naprav in ta ponuja Plasma Mobile kot možen vmesnik za naprave, ki jih podpira. Seznam podprtih naprav lahko vidite [tukaj](https://wiki.postmarketos.org/wiki/Devices), vendar se lahko v poljubni napravi zunaj glavnih kategorij in kategorij skupnosti razlikuje od dejanske podpore.

Vmesnik uporablja KWin na Waylandu in je zdaj večinoma stabilen, čeprav nekoliko nezlikan na nekaterih področjih. Na voljo je podskupina običajnih funkcij KDE Plasma, vključno z gradniki in dejavnostmi, ki sta integrirani v uporabniški vmesnik Plasma Mobile. To omogoča uporabo in razvoj za Plasma Mobile na namizju/prenosniku.

## Kaj lahko storim?

Na voljo je kar nekaj aplikacij, optimiziranih za dotik, ki so zdaj v paketu s sliko Plasma Mobile, ki temelji na Manjaru, kar omogoča širok nabor osnovnih funkcij.

Ti so večinoma zgrajeni s Kirigamijem, ogrodjem vmesnika KDE, ki omogoča konvergentne uporabniške vmesnike, ki delujejo zelo dobro v okoljih zgolj na dotik.

Seznam teh aplikacij najdete na [domači strani plasma-mobile.org](https://plasma-mobile.org).

## Kje lahko najdem…

Kodo za različne komponente Plasma Mobile najdete na [invent.kde.org](https://invent.kde.org/plasma-mobile).

Preberite [dokumentacijo za avtorje prispevkov](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) za več podrobnosti o skladu Plasma in o tem, kako vse to sodi skupaj.

Vprašanja lahko postavite tudi v [skupinah in kanalih skupnosti Plasma Mobile](/join).
