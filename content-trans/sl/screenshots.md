---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: Domači zaslon
  url: /screenshots/screenshot-2022-04-1.png
- name: Predal z aplikacijami (Mreža)
  url: /screenshots/screenshot-2022-04-2.png
- name: Predal z aplikacijami (Seznam)
  url: /screenshots/screenshot-2022-04-3.png
- name: Predal z aplikacijami (Razširjen)
  url: /screenshots/screenshot-2022-04-4.png
- name: Predal z aplikacijami (Strnjen)
  url: /screenshots/screenshot-2022-04-5.png
- name: Preklopnik dejavnosti
  url: /screenshots/screenshot-2022-04-6.png
- name: Vreme
  url: /screenshots/screenshot-2022-04-7.png
- name: Kalkulator
  url: /screenshots/screenshot-2022-04-8.png
- name: Koledar
  url: /screenshots/screenshot-2022-04-9.png
- name: Ura
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish, spletni brskalnik
  url: /screenshots/screenshot-2022-04-11.png
- name: Discover, skladišče aplikacij
  url: /screenshots/screenshot-2022-04-12.png
- name: Domači zaslon Plasma mobile
  url: /screenshots/screenshot-2022-04-13.png
- name: Index, upravljalnik datotek
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko, vpogledovalnik knjižnice slik
  url: /screenshots/screenshot-2022-04-15.png
- name: Številčnik
  url: /screenshots/screenshot-2022-04-16.png
- name: Nastavitve
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa, predvajalnik glasbe
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels, aplikacija kamere
  url: /screenshots/pp_camera.png
- name: Strojna oprema
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Zaslonske slike
---
Naslednji posnetki zaslona so bili posneti iz naprave Pinephone, ki poganja Plasma Mobile (April 2022)

{{< screenshots name="screenshots" >}}
