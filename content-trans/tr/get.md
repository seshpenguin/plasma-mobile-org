---
layout: get-involved
menu:
  main:
    name: Kur
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Plasma Mobile'i sunan dağıtımlar
---
Aşağıda Plasma Mobile yüklü gelen dağıtımları görebilirsiniz.

Aygıtınızın desteklenip desteklenmediğini görmek için her dağıtım için bilgileri denetleyin.

## Taşınabilir

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM, Manjaro dağıtımıdır; ancak ARM aygıtları içindir. ARM aygıtınız için yükleme kalıpları sağlayan Manjaro araçları, temaları ve altyapısı ile birlikte Arch Linux ARM'ye dayanmaktadır.

[Web sitesi](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

#### İndir

* [Güncel Kararlı (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [(Pinephone) Geliştirici Yapıları](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Kurulum

PinePhone için genel bilgiyi [Pine64 vikisinde](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions) bulabilirsiniz.

----

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS); akıllı telefonlara ve diğer taşınabilir aygıtlara kurulabilen, dokunmatik optimizasyonlu, önceden yapılandırılmış bir Alpine Linux'tur. Aygıtınızı destekleme konusundaki ilerlemeyi görmek için [aygıt listesini](https://wiki.postmarketos.org/wiki/Devices) görüntüleyin.

Önceden oluşturulmuş kalıplanı olmayan aygıtlar için, 'pmbootstrap' yardımcı programını kullanarak el ile flaşlamanız gerekecektir. [Buradaki](https://wiki.postmarketos.org/wiki/Installation_guide) talimatları uygulayın. Ayrıca neyin çalıştığı hakkında daha fazla bilgi için aygıtınızın viki sayfasını denetlediğinizden emin olun.

[Daha fazla öğrenin](https://postmarketos.org)

#### İndir

* [İyi desteklenen aygıtlar](https://postmarketos.org/download/)
* [Tam aygıt listesi](https://wiki.postmarketos.org/wiki/Devices)

----

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM, PinePhone ve PineTab'e DanctNIX topluluğu tarafından aktarılmıştır.

#### İndir

* [Yayımlar](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

----

### openSUSE

![](/img/openSUSE.svg)

openSUSE; eski adıyla SUSE Linux ve SuSE Linux Professional, SUSE Linux GmbH ve diğer şirketler tarafından desteklenen bir Linux dağıtımıdır. Şu anda openSUSE, Tumbleweed tabanlı Plasma Mobile yapıları sağlar.

#### İndir

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

----

### Fedora

![](/img/fedora.svg)

Hâlâ üzerinde çalışılmaktadır, bizi izlemeyi sürdürün!

İlerleme üzerine en güncel ayrıntıları almak için Fedora Mobility [matrix kanalına](https://matrix.to/#/#mobility:fedoraproject.org) katılın.

----

## Masaüstü Aygıtlar

### postmarketOS

![](/img/pmOS.svg)

postmarketOS, QEMU içinde çalıştırılabilir; böylelikle Plasma Mobile'i bilgisayarınızda kolaylıkla deneyebilirsiniz.

Ayrıntılı bilgi için [buraya](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)) bakın. Kurulum sırasında, masaüstü ortamı olarak Plasma Mobile'i seçin.

----

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile, [AUR](https://aur.archlinux.org/packages/plasma-mobile) üzerinde bulunabilir.

----

### Neon tabanlı amd64 ISO kalıbı

![](/img/neon.svg)

**UYARI**: Bu sürüm güncel değildir!

KDe neon tabanlı bu sürüm, Android olmayan Intel tabanlı tabletlerde, kişisel bilgisayarlarda ve sanal makinelerde sınanabilir.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
