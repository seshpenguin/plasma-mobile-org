---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Topluluk
---
Taşınabilir aygıtlar için tasarlanan bu mükemmel yazılıma katkıda bulunmak isterseniz [bize katılın, yapılacak bir şeyler her zaman vardır](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

----

Plasma Mobile topluluk grupları ve kanalları:

### Plasma Mobile'e özel kanallar:

* [![](/img/matrix.svg)Matrix (en yoğun)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Plasma Mobile posta listesi](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma Mobile ile ilgili proje kanalları:

* [![](/img/mail.svg)Plasma geliştirme posta listesi](https://mail.kde.org/mailman/listinfo/plasma-devel)
