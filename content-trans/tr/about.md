---
menu:
  main:
    parent: project
    weight: 2
title: Hakkında
---
Plasma Mobile; **KDE Plasma** temeli üzerine yapılmış, taşınabilir aygıtları hedefleyen bir açık kaynaklı kullanıcı arayüzü ve ekosistemidir.

Araç setinden bağımsız olarak yazılıma dahil olan ve kullanıcılara aygıtlarında kullanmak istedikleri yazılımı seçme gücü veren pragmatik bir yaklaşım benimsenmiştir.

Plasma Mobile, açık standartlara katkıda bulunmayı ve uygulamayı amaçlar ve herkesin katılımına açık olan saydam bir süreç ile geliştirilmiştir.

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## Nasıl kullanabilirim?

Evet! Plasma Mobile şu anda Manjaro ARM ile PinePhone'da önyüklü gelir. Ayrıca PinePhone için Plasma Mobile'i sunan başka dağıtımlar da mevcuttur.

Ayrıca daha birçok aygıtı destekleyen Alpine Linux tabanlı bir dağıtım olan postmarketOS da vardır ve desteklediği aygıtlar için uygun bir arayüz olarak Plasma Mobile'ı sunar. Desteklenen aygıtların listesini [burada](https://wiki.postmarketos.org/wiki/Devices) görebilirsiniz; ancak ana ve topluluk kategorilerinin dışındaki herhangi bir aygıtta bulacağınız deneyim farklı olabilir.

Arayüz, Wayland üzerinden KWin kullanır ve bazı kısımlarda biraz pürüzlü olsa da artık çoğunlukla kararlıdır. Her ikisi de Plasma Mobile Kullanıcı Arayüzüne entegre edilmiş araç takımları ve etkinlikler dahil olmak üzere normal KDE Plasma özelliklerinin bir alt kümesi mevcuttur. Bu, masaüstünüzde/dizüstü bilgisayarınızda Plasma Mobile kullanmayı ve onun için geliştirme yapmayı olanaklı kılar.

## Neler yapabilirim?

Şu anda Manjaro tabanlı Plasma Mobile kalıbıyla bir araya getirilen ve çok çeşitli temel işlevlere izin veren, dokunma için optimize edilmiş birkaç uygulama mevcuttur.

Bunlar çoğunlukla KDE'nin yalnızca dokunmatik bir ortamda çok iyi çalışan ve yakınsak grafik arabirimlere izin veren arayüz kod kitaplığı Kirigami ile oluşturulmuştur.

Bu uygulamaların bir listesini [plasma-mobile.org anasayfasında](https://plasma-mobile.org) bulabilirsiniz.

## Nerede bulabilirim…

Çeşitli Plasma Mobile bileşenleri için kodları [invent.kde.org](https://invent.kde.org/plasma-mobile) sayfasında bulabilirsiniz.

Plasma yığını üzerine ayrıntılı bilgi ve her şeyin nasıl çalıştığını görmek için [katkıda bulunma belgelerini](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) okuyun.

Sorularınızı ayrıca [Plasma Mobile topluluk grupları ve kanallarında](/join) da sorabilirsiniz.
