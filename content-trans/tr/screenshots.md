---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: Ana Ekran
  url: /screenshots/screenshot-2022-04-1.png
- name: Uygulama Çekmecesi (Izgara)
  url: /screenshots/screenshot-2022-04-2.png
- name: Uygulama Çekmecesi (Liste)
  url: /screenshots/screenshot-2022-04-3.png
- name: Eylem Çekmecesi (Genişletilmiş)
  url: /screenshots/screenshot-2022-04-4.png
- name: Eylem Çekmecesi (Küçültülmüş)
  url: /screenshots/screenshot-2022-04-5.png
- name: Görev Değiştirici
  url: /screenshots/screenshot-2022-04-6.png
- name: Hava Durumu
  url: /screenshots/screenshot-2022-04-7.png
- name: Hesap Makinesi
  url: /screenshots/screenshot-2022-04-8.png
- name: Takvim
  url: /screenshots/screenshot-2022-04-9.png
- name: Clock
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish, bir web tarayıcısı
  url: /screenshots/screenshot-2022-04-11.png
- name: Keşfet, bir uygulama mağazası
  url: /screenshots/screenshot-2022-04-12.png
- name: Plasma Mobile ana ekranı
  url: /screenshots/screenshot-2022-04-13.png
- name: Index, bir dosya yöneticisi
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko, bir fotoğraf kitaplığı görüntüleyici
  url: /screenshots/screenshot-2022-04-15.png
- name: Çevirici
  url: /screenshots/screenshot-2022-04-16.png
- name: Ayarlar
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa, bir müzik çalar
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels, bir kamera uygulaması
  url: /screenshots/pp_camera.png
- name: Donanım
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Ekran Görüntüleri
---
Aşağıdaki ekran görüntüleri, Plasma Mobile çalıştıran bir PinePhone'dan alınmıştır (Nisan 2022).

{{< screenshots name="screenshots" >}}
