---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: Домашня сторінка
  url: /screenshots/screenshot-2022-04-1.png
- name: Шухляда програм (таблиця)
  url: /screenshots/screenshot-2022-04-2.png
- name: Шухляда програм (список)
  url: /screenshots/screenshot-2022-04-3.png
- name: Шухляда програм (розгорнута)
  url: /screenshots/screenshot-2022-04-4.png
- name: Шухляда програм (згорнута)
  url: /screenshots/screenshot-2022-04-5.png
- name: Перемикання задач
  url: /screenshots/screenshot-2022-04-6.png
- name: Погода
  url: /screenshots/screenshot-2022-04-7.png
- name: Калькулятор
  url: /screenshots/screenshot-2022-04-8.png
- name: Календар
  url: /screenshots/screenshot-2022-04-9.png
- name: Годинник
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish, браузер
  url: /screenshots/screenshot-2022-04-11.png
- name: Discover, крамниця програм
  url: /screenshots/screenshot-2022-04-12.png
- name: Основний екран Плазми
  url: /screenshots/screenshot-2022-04-13.png
- name: Index, програма для керування файлами
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko, програма для перегляду фототеки
  url: /screenshots/screenshot-2022-04-15.png
- name: Засіб набирання
  url: /screenshots/screenshot-2022-04-16.png
- name: Параметри
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa, музичний програвач
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels, програма для роботи з камерою
  url: /screenshots/pp_camera.png
- name: Обладнання
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Знімки екрана
---
Наведені нижче знімки вікон було створено на пристрої Pinephone, на якому було запущено мобільну Плазму (квітень 2022 року).

{{< screenshots name="screenshots" >}}
