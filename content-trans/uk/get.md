---
layout: get-involved
menu:
  main:
    name: Встановити
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Дистрибутиви, які пропонують Plasma Mobile
---
Нижче наведено список дистрибутивів, які пропонують Plasma Mobile

Будь ласка, перевіряйте відомості щодо кожного з дистрибутивів — там наведено список підтримуваних пристроїв.

## Мобільні пристрої

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM є дистрибутивом Manjaro, але для пристроїв на основі ARM. Його засновано на Arch Linux ARM, поєднано із інструментами Manjaro, темами та інфраструктурою для встановлення образів для вашого пристрою ARM.

[Сайт](https://manjaro.org) [Форум](https://forum.manjaro.org/c/arm/)

#### Отримання

* [Найсвіжіша стабільна (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Збірки для розробників (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Встановлення

Щодо PinePhone, ви можете знайти загальні відомості у [вікі Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

----

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) — оптимізована для сенсорних пристроїв, попередньо налаштована версія Alpine Linux, яку можна встановити на смартфони та інші мобільні пристрої. Перегляньте [список пристроїв](https://wiki.postmarketos.org/wiki/Devices), щоб ознайомитися із поступом у підтримці вашого пристрою.

Для пристроїв, для яких немає попередньо зібраних образів, вам доведеться перешити систему пристрою за допомогою програми `pmbootstrap`. Виконайте настанови, які наведено [тут](https://wiki.postmarketos.org/wiki/Installation_guide). Не забудьте також ознайомитися зі сторінкою вікі пристрою, щоб дізнатися більше про те, що саме має працювати.

[Дізнатися більше](https://postmarketos.org)

#### Отримання

* [Підтримувані пристрої](https://postmarketos.org/download/)
* [Повний список пристроїв](https://wiki.postmarketos.org/wiki/Devices)

----

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM було портовано на PinePhone і PineTab спільнотою DanctNIX.

#### Отримання

* [Випуски](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

----

### openSUSE

![](/img/openSUSE.svg)

openSUSE, раніше SUSE Linux і SuSE Linux Professional, — дистрибутив Linux, який спонсоровано SUSE Linux GmbH та іншими компаніями. У поточній версії openSUSE надає доступ до заснованих на Tumbleweed збірках Plasma Mobile.

#### Отримання

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

----

### Fedora

![](/img/fedora.svg)

Робота триває — стежте за подіями!

Долучайтеся до [каналу matrix](https://matrix.to/#/#mobility:fedoraproject.org) Fedora Mobility, щоб дізнатися більше про поступ подій.

----

## Комп'ютерні пристрої

### postmarketOS

![](/img/pmOS.svg)

postmarketOS може працювати у QEMU, тому є придатним варіантом для того, щоб спробувати мобільну Плазму на вашому комп'ютері.

Спочатку ознайомтеся із [цією сторінкою](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). У процесі налаштовування просто виберіть як стільничне середовище мобільну Плазму.

----

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile доступна в [AUR](https://aur.archlinux.org/packages/plasma-mobile).

----

### Образ ISO для amd64 на основі Neon

![](/img/neon.svg)

**УВАГА!** Активний супровід не виконується!

Цей образ, зібраний на основі KDE neon, можна тестувати на планшетах intel без Android, персональних комп'ютерах та віртуальних машинах.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
