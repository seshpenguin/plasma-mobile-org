---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Спільнота
---
Якщо ви хочете взяти участь у розробці чудового вільного програмного забезпечення для мобільних пристроїв, [долучайтеся до нас — у нас завжди знайдеться для вас завдання](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

----

Групи і канали спільноти Plasma Mobile:

### Спеціалізовані канали Plasma Mobile:

* [![](/img/matrix.svg)Matrix (найактивніший канал)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Список листування Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Канали, які пов'язано із Plasma Mobile:

* [![](/img/mail.svg)Список листування розробників Плазми](https://mail.kde.org/mailman/listinfo/plasma-devel)
