---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Comunitat
---
Si us agradaria col·laborar amb l'impressionant programari lliure per a dispositius mòbils, [uniu-vos - sempre hi ha una tasca per a vós](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

----

Grups i canals de la comunitat Plasma Mobile:

### Canals específics del Plasma Mobile:

* [![](/img/matrix.svg)Matrix (més actiu)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Llista de correu del Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canals de projectes relacionats amb el Plasma Mobile:

* [![](/img/mail.svg)Llista de correu de desenvolupament del Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
