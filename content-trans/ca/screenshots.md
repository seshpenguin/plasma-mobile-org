---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: Pantalla d'inici
  url: /screenshots/screenshot-2022-04-1.png
- name: Calaix d'aplicacions (quadricula)
  url: /screenshots/screenshot-2022-04-2.png
- name: Calaix d'aplicacions (llista)
  url: /screenshots/screenshot-2022-04-3.png
- name: Calaix d'accions (ampliat)
  url: /screenshots/screenshot-2022-04-4.png
- name: Calaix d'accions (minimitzat)
  url: /screenshots/screenshot-2022-04-5.png
- name: Commutador de tasques
  url: /screenshots/screenshot-2022-04-6.png
- name: Meteorologia
  url: /screenshots/screenshot-2022-04-7.png
- name: Calculadora
  url: /screenshots/screenshot-2022-04-8.png
- name: Calendari
  url: /screenshots/screenshot-2022-04-9.png
- name: Rellotge
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish, un navegador web
  url: /screenshots/screenshot-2022-04-11.png
- name: Discover, una botiga d'aplicacions
  url: /screenshots/screenshot-2022-04-12.png
- name: Pantalla d'inici del Plasma Mobile
  url: /screenshots/screenshot-2022-04-13.png
- name: Index, un gestor de fitxers
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko, un visualitzador de biblioteques de fotos
  url: /screenshots/screenshot-2022-04-15.png
- name: Marcador
  url: /screenshots/screenshot-2022-04-16.png
- name: Configuració
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa, un reproductor de música
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels, una aplicació de càmera
  url: /screenshots/pp_camera.png
- name: El maquinari
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Captures de pantalla
---
Les captures de pantalla següents s'han pres des d'un dispositiu Pinephone executant el Plasma Mobile (abril 2022).

{{< screenshots name="screenshots" >}}
