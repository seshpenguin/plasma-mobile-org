---
menu:
  main:
    parent: project
    weight: 2
title: Quant al
---
El Plasma Mobile és una interfície d'usuari de codi obert i un ecosistema dirigit a dispositius mòbils, construït sobre la pila del **KDE Plasma**.

S'adopta un enfocament pragmàtic que és inclusiu per al programari independentment del joc d'eines, donant als usuaris el poder de triar el programari que vulguin utilitzar en el seu dispositiu.

El Plasma Mobile té com a objectiu contribuir i implementar estàndards oberts, i es desenvolupa en un procés transparent en el qual qualsevol pot participar.

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## El puc utilitzar?

Sí! Actualment el Plasma Mobile està preinstal·lat al PinePhone mitjançant el Manjaro ARM. També hi ha altres distribucions per al PinePhone amb el Plasma Mobile disponible.

També hi ha postmarketOS, una distribució basada en Alpine Linux amb suport per a molts més dispositius i ofereix el Plasma Mobile com a interfície disponible per als dispositius que suporta. Podeu veure la llista de dispositius compatibles [aquí](https://wiki.postmarketos.org/wiki/Devices), però en qualsevol dispositiu fora de les categoria principal i comunitària la vostra experiència pot variar.

La interfície utilitza el KWin sobre el Wayland i ara és majoritàriament estable, encara que una mica aspre a les parts secundàries en algunes àrees. Hi ha disponible un subconjunt de les característiques normals del Plasma del KDE, incloent ginys i activitats, ambdues estan integrades a la interfície d'usuari del Plasma Mobile. Això fa que sigui possible utilitzar i desenvolupar el Plasma Mobile a l'escriptori/portàtil.

## Què puc fer?

Hi ha bastants aplicacions optimitzades en tacte que ara s'estan empaquetant amb la imatge del Plasma Mobile basada en Manjaro, permetent una àmplia varietat de funcions bàsiques.

Aquests es construeixen majoritàriament amb el Kirigami, el marc de treball de la interfície del KDE que permet IU convergents que funcionen molt bé en un entorn només tàctil.

Podeu trobar una llista d'aquestes aplicacions a la [pàgina web plasma-mobile.org](https://plasma-mobile.org).

## A on el puc trobar…

El codi per a diversos components del Plasma Mobile es pot trobar a [invent.kde.org](https://invent.kde.org/plasma-mobile).

Llegiu la [documentació dels col·laboradors](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) per a més detalls sobre la pila del Plasma i com encaixa tot.

També podeu preguntar les vostres qüestions als [grups i canals de la comunitat Plasma Mobile](/join).
