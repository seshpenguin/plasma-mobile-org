---
layout: get-involved
menu:
  main:
    name: Instalatu
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Plasma Mobile eskaintzen duten banaketak
---
Listed below are distributions that ship Plasma Mobile.

Please check the information for each distribution to see if your device is supported.

## Mobile

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM, ARM gailuetarako Manjaro banaketa da. Arch Linux ARM-en oinarrituta dago, Manjaro tresnak, gaiak eta azpiegiturarekin konbinatuta, zure ARM gailuetarako irudiak instalatzeko.

[Webgunea](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

#### Zama-jaitsi

* [Azken egonkorra (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Garatzaileentzako eraikinak] (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Instalazioa

PinePhone-rako, informazio generikoa aurki dezakezu [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions)an.

----

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), telefono adimendunetan eta beste gailu mugikorretan instalatu daitekeen, ukimenerako optimizatutako, aurre-konfiguratutako Alpine Linux bat da. Begiratu [gailu-zerrenda](https://wiki.postmarketos.org/wiki/Devices) zure gailuak euskarria izateko aurrerapena ikusteko.

Aurre-eraikitako irudirik ez duten gailuetarako, «pmbootstrap» erabili beharko duzu irudia flash memorian eskuz grabatzeko. Jarraitu [honako](https://wiki.postmarketos.org/wiki/Installation_guide) jarraibideak. Egiaztatu gailuaren wiki orria funtzionatzen ari denari buruzko informazio gehiago lortzeko.

[Jakizu gehiago](https://postmarketos.org)

#### Zama-jaitsi

* [Euskarri ona duten gailuak](https://postmarketos.org/download/)
* [Gailuen zerrenda osoa](https://wiki.postmarketos.org/wiki/Devices)

----

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM has been ported to the PinePhone and PineTab by the DanctNIX community.

#### Zama-jaitsi

* [Releases](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

----

### openSUSE

![](/img/openSUSE.svg)

openSUSE, behinola SUSE Linux eta SuSE Linux Professional, SUSE Linux GmbH-ek eta beste konpainia batzuek babestutako Linux banaketa bat da. Gaur egun, openSUSE-k Tumbleweed oinarri duten Plasma Mobile eraikinak hornitzen ditu.

#### Zama-jaitsi

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

----

### Fedora

![](/img/fedora.svg)

This is a work in progress, stay tuned!

Join the Fedora Mobility [matrix channel](https://matrix.to/#/#mobility:fedoraproject.org) to get details on the progress.

----

## Mahaigaineko gailuak

### postmarketOS

![](/img/pmOS.svg)

postmarketOS QEMUn ibil daiteke, eta beraz Plasma Mobile zure ordenagailuan probatzeko aukera aproposa da.

Irakurri gehiago honi buruz [hemen](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). Ezartzeko prozesuan, mahaigaineko ingurune gisa Plasma Mobile aukeratu besterik ez da egin behar.

----

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile is now available on the [AUR](https://aur.archlinux.org/packages/plasma-mobile).

----

### Neon oinarriko amd64 ISO irudia

![](/img/neon.svg)

**WARNING**: This is not actively maintained!

KDE Neon-en oinarritutako irudi hau, android ez darabilten intel tabletatan, PCetan eta alegiazko makinetan proba daiteke.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
