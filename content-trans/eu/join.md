---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Komunitatea
---
If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

----

Plasma Mobile komunitateko taldeak eta kanalak:

### Plasma Mobilen berariazko kanalak:

* [![](/img/matrix.svg)Matrix (aktiboena)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma Mobilekin zerikusia duten proiektuen kanalak:

* [![](/img/mail.svg)Plasma garapenerako posta zerrenda](https://mail.kde.org/mailman/listinfo/plasma-devel)
