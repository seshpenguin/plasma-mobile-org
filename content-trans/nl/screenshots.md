---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
minJsFiles:
- js/swiper-bundle.min.js
screenshots:
- name: Standaardscherm
  url: /screenshots/screenshot-2022-04-1.png
- name: Lade van toepassing (raster)
  url: /screenshots/screenshot-2022-04-2.png
- name: Lade van toepassing (lijst)
  url: /screenshots/screenshot-2022-04-3.png
- name: Actielade (uitgevouwen)
  url: /screenshots/screenshot-2022-04-4.png
- name: Actielade (geminimaliseerd)
  url: /screenshots/screenshot-2022-04-5.png
- name: Taakschakelaar
  url: /screenshots/screenshot-2022-04-6.png
- name: Weer
  url: /screenshots/screenshot-2022-04-7.png
- name: Rekenmachine
  url: /screenshots/screenshot-2022-04-8.png
- name: Agenda
  url: /screenshots/screenshot-2022-04-9.png
- name: Klok
  url: /screenshots/screenshot-2022-04-10.png
- name: Angelfish, een webbrowser
  url: /screenshots/screenshot-2022-04-11.png
- name: Discover, een toepassingen-store
  url: /screenshots/screenshot-2022-04-12.png
- name: Plasma mobile startscherm
  url: /screenshots/screenshot-2022-04-13.png
- name: Index, een bestandsbeheerder
  url: /screenshots/screenshot-2022-04-14.png
- name: Koko, een viewer van een fotobibliotheek
  url: /screenshots/screenshot-2022-04-15.png
- name: Telefoonkiezer
  url: /screenshots/screenshot-2022-04-16.png
- name: Instellingen
  url: /screenshots/screenshot-2022-04-17.png
- name: Elisa, een muziekspeler
  url: /screenshots/screenshot-2022-04-18.png
- name: Megapixels, een camera-toepassing
  url: /screenshots/pp_camera.png
- name: De hardware
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Schermafdrukken
---
De volgende schermafdrukken zijn genomen van een Pinephone apparaat waarop Plasma Mobile draait (april 2022).

{{< screenshots name="screenshots" >}}
