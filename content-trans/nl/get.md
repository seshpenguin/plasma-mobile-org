---
layout: get-involved
menu:
  main:
    name: Installeren
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Distributies die Plasma Mobile bieden
---
In de onderstaande lijst staan distributies die Plasma-mobiel bieden

Bekijk de informatie voor elke distributie om te zien of uw apparaat wordt ondersteund.

## Mobiel

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM is de Manjaro distributie, maar voor ARM apparaten. Het is gebaseerd op Arch Linux ARM, gecombineerd met Manjaro hulpmiddelen, thema's en infrastructuur om images te installeren voor uw ARM apparaat.

[Website](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

#### Downloaden

* [Laatste Stabiele (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Developer builds (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Installatie

Voor de PinePhone kunt u algemene informatie vinden op [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

----

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), is een voor aanraken geoptimaliseerd, voorgeconfigureerd Alpine Linux dat geïnstalleerd kan worden op smartphones en andere mobiele apparaten.  Bekijk de [apparaatlijst](https://wiki.postmarketos.org/wiki/Devices) om de voortgang voor ondersteuning van uw apparaat te zien.

Voor apparaten die geen vooraf gebouwde images hebben, moet u het handmatig flashen met het hulpmiddel `pmbootstrap`. Volg instructies [hier](https://wiki.postmarketos.org/wiki/Installation_guide). Bekijk ook de wiki-pagina van het apparaat voor meer informatie over wat er werkt.

[Meer weten](https://postmarketos.org)

#### Downloaden

* [Goed ondersteunde apparaten](https://postmarketos.org/download/)
* [Volledige lijst van apparaten](https://wiki.postmarketos.org/wiki/Devices)

----

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM is overgebracht naar de PinePhone en PineTab door de DanctNIX gemeenschap.

#### Downloaden

* [Uitgaven](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

----

### openSUSE

![](/img/openSUSE.svg)

openSUSE, vroeger SUSE Linux en SuSE Linux Professional, is een Linux distributie gesponserd door SUSE Linux GmbH en andere bedrijven. openSUSE levert nu Tumbleweed gebaseerd op Plasma Mobile builds.

#### Downloaden

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

----

### Fedora

![](/img/fedora.svg)

Dit is Werk in uitvoering, blijf kijken!

Doe mee met de Fedora Mobility [matrix-kanaal](https://matrix.to/#/#mobility:fedoraproject.org) om details over de voortgang te krijgen.

----

## Bureaublad apparaten

### postmarketOS

![](/img/pmOS.svg)

postmarketOS kan in QEMU uitgevoerd worden en is dus een geschikte optie voor het proberen van Plasma Mobile op uw computer.

Lees er [hier](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)) meer over. Tijdens het opzetten, selecteer gewoon Plasma Mobile als de bureaubladomgeving.

----

### Arch Linux

![](/img/archlinux.png)

Plasma-mobiel is nu beschikbaar op de [AUR](https://aur.archlinux.org/packages/plasma-mobile).

----

### Neon op amd64 gebaseerde ISO image

![](/img/neon.svg)

**WAARSCHUWING**: dit wordt niet actief onderhouden!

Deze image, gebaseerd op KDE neon, kan getest worden op niet-android intel-tablets, PC's en virtuele machines.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
