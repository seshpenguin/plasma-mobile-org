---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Gemeenschap
---
Als u wilt bijdragen aan de verbazingwekkende vrije software voor mobiele apparaten, [doe mee - we hebben altijd een taak voor u](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

----

Plasma Mobile gemeenschapsgroepen en kanalen:

### Plasma Mobile specifieke kanalen:

* [![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma Mobile gerelateerde projectkanalen:

* [![](/img/mail.svg)Plasma ontwikkeling e-maillijst](https://mail.kde.org/mailman/listinfo/plasma-devel)
