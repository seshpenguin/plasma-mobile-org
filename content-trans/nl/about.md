---
menu:
  main:
    parent: project
    weight: 2
title: Info over
---
Plasma-mobiel is een open-source gebruikersinterface en ecosysteem gericht op mobiele apparaten, gebouwd op de **KDE Plasma** stack.

Er is een pragmatische benadering genomen dat inclusief is naar software ongeacht de toolkit, waarmee gebruikers de mogelijkheid krijgen om welke software dan ook te kiezen, die ze willen gebruiken op hun apparaat.

Plasma-mobiel richt zich op het bijdragen aan en implementeren van open standaarden en wordt ontwikkeld in een transparent proces dat open is voor iedereen om in te participeren.

<img src="/img/konqi/konqi-calling.png" width=250px/>

----

## Kan ik het gebruiken?

Ja! Plasma-mobiel is nu voorgeïnstalleerd op de PinePhone met gebruik van Manjaro ARM. Er zijn ook verschillende andere distributies voor de PinePhone met Plasma-mobiel beschikbaar.

Er is ook postmarketOS, een Alpine op Linux gebaseerde distributie met ondersteuning voor veel meer apparaten en het biedt Plasma-mobiel als een beschikbaar interface voor de apparaten die het ondersteunt. U kunt de lijst met ondersteunde apparaten [hier](https://wiki.postmarketos.org/wiki/Devices) zien, maar op elk apparaat buiten de hoofd- en gemeenschapscategorieën zal het variëren.

Het interface gebruikt KWin over Wayland en is nu meestal stabiel, hoewel het kleine ruwe randjes heeft in sommige gebieden. Een subset van de normale KDE Plasma functies zijn beschikbaar, inclusief widgets en activiteiten, beiden zijn geïntegreerd in de Plasma-mobiel UI. Dit maakt het mogelijk om Plasma-mobiel op uw desktop/laptop te gebruiken en te ontwikkelen.

## Wat kan het doen?

Er zijn aardig wat voor aanraken geoptimaliseerde toepassingen die nu gebundeld worden met de op Manjaro gebaseerde Plasma-mobiel image, waarmee het een brede reeks van basisfuncties biedt.

Deze zijn meestal gebouwd met Kirigami, het interfaceframework van KDE, waarmee convergente UI's worden geboden die zeer goed werken in een omgeving met alleen aanraken.

U kunt een lijst met deze toepassingen vinden op [de homepagina van plasma-mobile.org](https://plasma-mobile.org).

## Waar kan ik vinden…

De code voor verschillende Plasma-mobiel componenten zijn te vinden op [invent.kde.org](https://invent.kde.org/plasma-mobile).

Lees de [documentatie voor hen doe bijdragen](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) voor meer details over de Plasma stack en hoe alles in elkaar steekt.

U kunt ook vragen stellen in de [Plasma-mobiel gemeenschapsgroepen en kanalen](/join).
